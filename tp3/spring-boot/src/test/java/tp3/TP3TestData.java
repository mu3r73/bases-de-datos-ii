package tp3;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import tp3.model.Cliente;
import tp3.model.Factura;
import tp3.model.Proveedor;
import tp3.model.producto.Congelado;
import tp3.model.producto.Frio;
import tp3.model.producto.General;
import tp3.model.producto.Gondola;
import tp3.model.producto.Producto;
import tp3.service.ClienteService;
import tp3.service.ProductoService;
import tp3.service.ProveedorService;

public class TP3TestData {

  /**
   * datos de prueba
   */
  
  public static List<Cliente> getClientes() {
    List<Cliente> cs = new ArrayList<>();
    cs.add(new Cliente("C001", "Giménez", "Verna Lucrecia", "7712"));
    cs.add(new Cliente("C002", "Aráoz", "Héctor", "3561"));
    cs.add(new Cliente("C003", "Chávez", "Alicia", "9906"));
    cs.add(new Cliente("C004", "Velazco", "Evaristo Gregorio", "5829"));
    return cs;
  }
  
  public static List<Proveedor> getProveedores() {
    List<Proveedor> ps = new ArrayList<>();
    ps.add(new Proveedor("Mayorista Urris"));
    ps.add(new Proveedor("Carcor S.R.L."));
    ps.add(new Proveedor("Hadén S.A."));
    ps.add(new Proveedor("Quetmata Mayorista"));
    return ps;
  }
  
  public static List<Frio> getFrios(ProveedorService ps) {
    List<Frio> fs = new ArrayList<>();
    
    fs.add(new Frio("P001", "Manteca 200g", 
                          ps.findByDesc("Quetmata Mayorista"),
                          Optional.of(BigDecimal.valueOf(125)), 2, 4)); 
    
    fs.add(new Frio("P002", "Yogur bebible 1l",
                          ps.findByDesc("Quetmata Mayorista"), 
                          Optional.of(BigDecimal.valueOf(87)), 2, 8));
    
    return fs;
  }
  
  public static List<Congelado> getCongelados(ProveedorService ps) {
    List<Congelado> cs = new ArrayList<>();
    
    cs.add(new Congelado("P003", "Bocaditos de carne 380g",
                           ps.findByDesc("Carcor S.R.L."),
                           Optional.of(BigDecimal.valueOf(173)))); 
        
    cs.add(new Congelado("P004", "Mix de frutos del bosque congelados 1kg",
                           ps.findByDesc("Carcor S.R.L."),
                           Optional.of(BigDecimal.valueOf(203))));
    
    return cs;
  }

  public static List<Gondola> getGondolas(ProveedorService ps) {
    List<Gondola> gs = new ArrayList<>();
    
    gs.add(new Gondola("P005", "Aceite de girasol 1.5l",
                          ps.findByDesc("Mayorista Urris"),
                          Optional.of(BigDecimal.valueOf(148)), 1500)); 
        
    gs.add(new Gondola("P006", "Harina 0000 1kg",
                          ps.findByDesc("Mayorista Urris"),
                          Optional.of(BigDecimal.valueOf(48)), 1000));
    
    return gs;
  }
  
  public static List<General> getGenerales(ProveedorService ps) {
    List<General> gs = new ArrayList<>();
    
    gs.add(new General("P007", "Secador de goma",
                          ps.findByDesc("Hadén S.A."),
                          Optional.of(BigDecimal.valueOf(46)), 170)); 
        
    gs.add(new General("P008", "Quitamanchas 500cc",
                          ps.findByDesc("Hadén S.A."),
                          Optional.of(BigDecimal.valueOf(70)), 550));
    
    gs.add(new General("P009", "Mochila escolar 25.3l",
                          ps.findByDesc("Hadén S.A."),
                          Optional.of(BigDecimal.valueOf(2390)), 300));
    
    gs.add(new General("P010", "Detergente en polvo 10 kg",
                          ps.findByDesc("Hadén S.A."),
                          Optional.of(BigDecimal.valueOf(1657)), 10000));
    
    gs.add(new General("P011", "Vino Merlot importado de Italia cosecha 2005 750ml",
                          ps.findByDesc("Hadén S.A."),
                          Optional.of(BigDecimal.valueOf(10304)), 1450));
    
    return gs;
  }

  public static Factura getFactura(ClienteService cs, ProductoService ps) {
    Cliente c = cs.findByCodigo("C003");
    
    Factura f = new Factura(c, 1);
    
    Producto p1 = ps.findByCodigo("P002");
    f.addDetalle(p1, 1);
    
    Producto p2 = ps.findByCodigo("P005");
    f.addDetalle(p2, 1);
    
    Producto p3 = ps.findByCodigo("P007");
    f.addDetalle(p3, 3);
    
    return f;
  }
  
}
