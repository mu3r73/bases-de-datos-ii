package tp3.no_pedidos;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tp3.TP3TestData;
import tp3.dto.ClienteDTO;
import tp3.model.Cliente;
import tp3.repo.ClienteRepository;
import tp3.service.ClienteService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TP1Ej0TestAltaClienteDTOs {
  
  @Autowired
  ClienteService cs;
  
  @Autowired
  ClienteRepository cr;
  
  @Test
  public void altaClientes() {
    System.out.println(">>> tp3ej0: alta de clientes");
    
    List<Cliente> cs = TP3TestData.getClientes();
    for (Cliente c : cs) {
      ClienteDTO cDTO = this.cs.create(new ClienteDTO(c));
      c.setId(cDTO.getId());
    }
    
    validarCarga(cs);
  }
  
  private void validarCarga(List<Cliente> cs) {
    System.out.println(">>> tp3ej0: consulta de clientes");
    
    // todos los clientes en cs están cargados
    List<Cliente> cliRes = new ArrayList<>();
    this.cr.findAll().forEach(c -> cliRes.add(c));
    for (Cliente c : cs) {
      Assert.assertTrue(cliRes.contains(c));
    }
    // hay un total de 4 clientes
    Assert.assertEquals(cs.size(), cliRes.size());
  }

}
