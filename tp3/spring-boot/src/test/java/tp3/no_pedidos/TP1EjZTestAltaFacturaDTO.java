package tp3.no_pedidos;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tp3.TP3TestData;
import tp3.dto.FacturaDTO;
import tp3.model.Detalle;
import tp3.model.Factura;
import tp3.repo.DetalleRepository;
import tp3.service.ClienteService;
import tp3.service.FacturaService;
import tp3.service.ProductoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TP1EjZTestAltaFacturaDTO {

  @Autowired
  private FacturaService fs;

  @Autowired
  private ClienteService cs;
  
  @Autowired
  private ProductoService ps;

  @Autowired
  private DetalleRepository dr;

  @Test
  public void altaFactura() {
    System.out.println(">>> tp3ejZ: alta de una factura");
    
    Factura f = TP3TestData.getFactura(cs, ps);
    
    FacturaDTO fDTO = this.fs.create(new FacturaDTO(f)); 
    
    f.setId(fDTO.getId());
    
    validarCarga(f);
  }

  private void validarCarga(Factura f) {
    System.out.println(">>> tp3ejZ: consulta de factura");
    Factura fRes = fs.findByNumero(f.getNumero());
    // el cliente de fRes es el mismo que se cargó en f
    Assert.assertEquals(fRes.getCliente(), f.getCliente());
    
    System.out.println(">>> tp3ejZ: consulta de detalles");
    List<Detalle> ds = this.dr.findByFactura(f);
    // hay un detalle para cada detalle en f
    for (Detalle d : f.getDetalles()) {
      // chequeo por match de producto y cantidad
      Assert.assertTrue(ds.stream().anyMatch(det -> 
        det.getProducto().equals(d.getProducto()) 
        && det.getCantidad().equals(d.getCantidad())
      ));
    }
    // los precios son correctos
    for (Detalle d : ds) {
      Assert.assertTrue(d.getPrecio().equals(d.getProducto().getPrecio()));
    } 
  }
  
}
