package tp3;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import tp3.model.Proveedor;
import tp3.service.ProveedorService;

@RunWith(SpringRunner.class)
@SpringBootTest
// comentar la línea que sigue para NO recrear las tablas
@TestPropertySource("classpath:create.properties")
public class TP3Ej2TestAltaProveedores {
  
  @Autowired
  private ProveedorService ps;

  @Test
  public void altaProveedores() {
    System.out.println(">>> tp3ej2: alta de proveedores");
    
    List<Proveedor> provs = TP3TestData.getProveedores();
    
    for (Proveedor p : provs) {
      this.ps.create(p);      
    }
    
    this.validarCarga(provs);
  }

  private void validarCarga(List<Proveedor> provs) {
    System.out.println(">>> tp3ej2: consulta de proveedores");
    
    // todos los proveedores en ps están cargados
    List<Proveedor> provRes = this.ps.findAll();
    for (Proveedor p : provs) {
      Assert.assertTrue(provRes.contains(p));
    }
    // hay un total de 4 proveedores
    Assert.assertEquals(provs.size(), provRes.size());
  }

}
