package tp3;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tp3.model.producto.Congelado;
import tp3.model.producto.Frio;
import tp3.model.producto.General;
import tp3.model.producto.Gondola;
import tp3.model.producto.Producto;
import tp3.service.CongeladoService;
import tp3.service.FrioService;
import tp3.service.GeneralService;
import tp3.service.GondolaService;
import tp3.service.ProductoService;
import tp3.service.ProveedorService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TP3Ej3TestAltaProductos {

  @Autowired
  private FrioService fs;
  
  @Autowired
  private CongeladoService cs;

  @Autowired
  private GondolaService gs;

  @Autowired
  private GeneralService gens;

  @Autowired
  private ProductoService ps;
  
  @Autowired
  private ProveedorService provss;


  
  @Test
  public void altaProductos() {
    System.out.println(">>> tp3ej3: alta de productos");
    
    // carga de productos por tipo
    List<Frio> frios = TP3TestData.getFrios(this.provss);
    for (Frio f : frios) {
      this.fs.create(f);
    }
    
    List<Congelado> congelados = TP3TestData.getCongelados(this.provss);
    for (Congelado c : congelados) {
      this.cs.create(c);
    }
    
    List<Gondola> gondolas = TP3TestData.getGondolas(this.provss);
    for (Gondola g : gondolas) {
      this.gs.create(g);
    }
    
    List<General> generales = TP3TestData.getGenerales(this.provss);
    for (General g : generales) {
      this.gens.create(g);
    }
    
    // resultado esperado = fríos + congelados + góndola + generales
    List<Producto> prods = new ArrayList<>();
    prods.addAll(frios);
    prods.addAll(congelados);
    prods.addAll(gondolas);
    prods.addAll(generales);
    
    this.validarCarga(prods);    
  }

  private void validarCarga(List<Producto> prods) {
    System.out.println(">>> tp3ej3: consulta de productos");
    // resultado de consulta
    List<Producto> prodsRes = this.ps.findAll();
    
    // verifico que ambas listas tengan el mismo tamaño
    Assert.assertEquals(prods.size(), prodsRes.size());
    
    // verifico que cada producto en prods esté en prodsRes
    for (Producto p : prods) {
      Assert.assertTrue(prodsRes.contains(p));
    }

    // verifico que cada producto en prodsRes esté en prods
    for (Producto p : prodsRes) {
      Assert.assertTrue(prods.contains(p));
    }
  }
  
}
