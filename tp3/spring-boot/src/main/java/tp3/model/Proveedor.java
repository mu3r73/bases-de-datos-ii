package tp3.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import tp3.model.producto.Producto;


@Entity
@Table(name = "proveedor")
public class Proveedor {
  
  @Id
  @GeneratedValue
  private long id;  

  @Column(nullable = false)
  private String descripcion;  

  @ManyToMany(mappedBy = "proveedores", cascade = CascadeType.ALL)
  private Collection<Producto> productos = new ArrayList<>();

  // constructores
  public Proveedor() {
    super();
  }

  public Proveedor(String descripcion) {
    super();
    this.descripcion = descripcion;
  }

  // overrides
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (!(obj instanceof Proveedor))
      return false;
    Proveedor other = (Proveedor) obj;
    return Objects.equals(id, other.id);
  }
  
  @Override
  public String toString() {
    return this.descripcion;
  }

  // getters y setters
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public Collection<Producto> getProductos() {
    return productos;
  }

  public void setProductos(Collection<Producto> productos) {
    this.productos = productos;
  }

}
