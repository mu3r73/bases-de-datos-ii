package tp3.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import tp3.model.producto.Producto;

@Entity
@Table(name = "factura")
public class Factura {

  @Id
  @GeneratedValue
  private long id;

  @ManyToOne
  @JoinColumn(name = "fk_id_cliente", referencedColumnName = "id")
  private Cliente cliente;

  @Column(nullable = false)
  private LocalDateTime fecha;

  @Column(nullable = false, unique = true, updatable = false)
  private int numero;

  @OneToMany(mappedBy = "factura", cascade = CascadeType.ALL)
  private Collection<Detalle> detalles = new ArrayList<>();

  // constructores
  public Factura() {
    super();
  }

  public Factura(Cliente cliente, int numero) {
    super();
    this.cliente = cliente;
    this.fecha = LocalDateTime.now();
    this.numero = numero;
  }

  // overrides
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (!(obj instanceof Factura))
      return false;
    Factura other = (Factura) obj;
    return Objects.equals(id, other.id);
  }

  // getters y setters
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Cliente getCliente() {
    return cliente;
  }

  public void setCliente(Cliente cliente) {
    this.cliente = cliente;
  }

  public LocalDateTime getFecha() {
    return fecha;
  }

  public void setFecha(LocalDateTime fecha) {
    this.fecha = fecha;
  }

  public Integer getNumero() {
    return numero;
  }

  public void setNumero(Integer numero) {
    this.numero = numero;
  }

  public Collection<Detalle> getDetalles() {
    return detalles;
  }

  public void setDetalles(Collection<Detalle> detalles) {
    this.detalles = detalles;
  }

  // convenience
  public void addDetalle(Producto producto, int cantidad) {
    Detalle detalle = new Detalle(this, producto, cantidad, producto.getPrecio());
    this.getDetalles().add(detalle);
  }

}
