package tp3.model.producto;

import java.math.BigDecimal;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.Table;

import tp3.model.Proveedor;


@Entity
@Table(name = "congelado")
public class Congelado extends Alimento {

  // constructores
  public Congelado() {
    super();
  }
  
  public Congelado(String codigo, String descripcion, Proveedor proveedor, Optional<BigDecimal> monto) {
    super(codigo, descripcion, proveedor, monto);
  }

  // overrides
  @Override
  public BigDecimal getPrecioFinal() {
    BigDecimal monto = this.getPrecio().getMonto();
    BigDecimal incremento = monto.multiply(BigDecimal.valueOf(8)).divide(BigDecimal.valueOf(100));
    return monto.add(incremento);
  }

}
