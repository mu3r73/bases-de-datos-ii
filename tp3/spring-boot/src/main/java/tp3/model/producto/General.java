package tp3.model.producto;

import java.math.BigDecimal;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import tp3.model.Proveedor;


@Entity
@Table(name = "general")
public class General extends Producto {

  @Column(nullable = false, updatable = false)
  private Integer peso; // en g

  // constructores
  public General() {
    super();
  }
  
  public General(String codigo, String descripcion, Proveedor proveedor, Optional<BigDecimal> monto,
      Integer peso) {
    super(codigo, descripcion, proveedor, monto);
    this.peso = peso;
  }

  // getters y setters
  public Integer getPeso() {
    return peso;
  }

  public void setPeso(Integer peso) {
    this.peso = peso;
  }

  // overrides
  @Override
  public BigDecimal getPrecioFinal() {
    BigDecimal monto = this.getPrecio().getMonto();
    BigDecimal incremento = BigDecimal.valueOf(0); 
    if (this.peso >= 2 && this.peso <= 4) {
      incremento = monto.multiply(BigDecimal.valueOf(4)).divide(BigDecimal.valueOf(100));
    } else if (this.peso > 4 && this.peso <= 7) {
      incremento = monto.multiply(BigDecimal.valueOf(7)).divide(BigDecimal.valueOf(100));
    } else if (this.peso > 7) {
      incremento = monto.multiply(BigDecimal.valueOf(12)).divide(BigDecimal.valueOf(100));
    }
    return monto.add(incremento);
  }

}
