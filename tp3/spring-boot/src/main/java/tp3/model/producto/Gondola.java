package tp3.model.producto;

import java.math.BigDecimal;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import tp3.model.Proveedor;


@Entity
@Table(name = "gondola")
public class Gondola extends Alimento {

  @Column(nullable = false, updatable = false)
  private Integer volumen; // en cm³
  
  // constructores
  public Gondola() {
    super();
  }
  
  public Gondola(String codigo, String descripcion, Proveedor proveedor, Optional<BigDecimal> monto,
      Integer volumen) {
    super(codigo, descripcion, proveedor, monto);
    this.volumen = volumen;
  }
  
  // getters y setters  
  public Integer getVolumen() {
    return volumen;
  }

  public void setVolumen(Integer volumen) {
    this.volumen = volumen;
  }

  // overrides
  @Override
  public BigDecimal getPrecioFinal() {
    BigDecimal monto = this.getPrecio().getMonto();
    BigDecimal incremento = (this.volumen > 400)
        ? monto.multiply(BigDecimal.valueOf(3)).divide(BigDecimal.valueOf(100))
        : BigDecimal.valueOf(0);
    return monto.add(incremento);
  }

}
