package tp3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tp3.dto.FacturaDTO;
import tp3.service.FacturaService;

@RestController
@RequestMapping("/api/factura")
public class FacturaController {

  @Autowired
  private FacturaService fs;

  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<FacturaDTO> create(@RequestBody FacturaDTO fDTO) {
    fDTO = this.fs.create(fDTO);
    return new ResponseEntity<FacturaDTO>(fDTO, HttpStatus.CREATED);
  }

}
