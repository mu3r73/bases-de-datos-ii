package tp3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tp3.dto.ClienteDTO;
import tp3.service.IClienteService;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {

  @Autowired
  private IClienteService cs;

  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<ClienteDTO> create(@RequestBody ClienteDTO cDTO) {
    cDTO = this.cs.create(cDTO);
    return new ResponseEntity<ClienteDTO>(cDTO, HttpStatus.CREATED);
  }

  @RequestMapping(value = "/findById/{id}", method = RequestMethod.GET)
  public ResponseEntity<ClienteDTO> findByID(@PathVariable("id") long id) throws NotFoundException {
    ClienteDTO cDTO = this.cs.findById(id);
    if (cDTO == null) {
      throw new NotFoundException();
    }
    return new ResponseEntity<ClienteDTO>(cDTO, HttpStatus.OK);
  }
  
  @RequestMapping(method = RequestMethod.GET)
  public ResponseEntity<List<ClienteDTO>> findAll() {
    List<ClienteDTO> cs = this.cs.findAll();
    return new ResponseEntity<List<ClienteDTO>>(cs, HttpStatus.OK);
  }
  
  @RequestMapping(value = "/update", method = RequestMethod.PUT)
  public ResponseEntity<ClienteDTO> update(@RequestBody ClienteDTO cDTO) throws NotFoundException {
    cDTO = this.cs.update(cDTO);
    if (cDTO == null) {
      throw new NotFoundException();
    }
    return new ResponseEntity<ClienteDTO>(cDTO, HttpStatus.OK);
  }
  
  @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<Void>  delete(@PathVariable("id") long id) throws NotFoundException {
    ClienteDTO cDTO = this.cs.findById(id);
    if (cDTO == null) {
      throw new NotFoundException();
    }
    this.cs.delete(id);
    return new ResponseEntity<Void>(HttpStatus.OK);
  }
  
  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<Void> exceptionHandler(Exception e){
    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
  }

}
