package tp3.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import tp3.model.Factura;

public interface FacturaRepository extends CrudRepository<Factura, Long> {
  // tipo de dato y primary key
  
  List<Factura> findByNumero(int numero);
  // Spring genera automáticamente el código de la consulta
  
}
