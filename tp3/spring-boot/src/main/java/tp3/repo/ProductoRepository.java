package tp3.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import tp3.model.producto.Producto;

public interface ProductoRepository extends CrudRepository<Producto, Long> {
  // tipo de dato y primary key
  
  List<Producto> findByCodigo(String codigo);
  // Spring genera automáticamente el código de la consulta
  
}
