package tp3.repo;

import org.springframework.data.repository.CrudRepository;

import tp3.model.producto.General;

public interface GeneralRepository extends CrudRepository<General, Long> {
  // tipo de dato y primary key
}
