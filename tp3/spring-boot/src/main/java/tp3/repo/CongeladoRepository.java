package tp3.repo;

import org.springframework.data.repository.CrudRepository;

import tp3.model.producto.Congelado;

public interface CongeladoRepository extends CrudRepository<Congelado, Long> {
  // tipo de dato y primary key
}
