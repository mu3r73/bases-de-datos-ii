package tp3.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import tp3.model.Detalle;
import tp3.model.Factura;

public interface DetalleRepository extends CrudRepository<Detalle, Long> {
  // tipo de dato y primary key
  
  List<Detalle> findByFactura(Factura factura);
  // Spring genera automáticamente el código de la consulta
  
}
