package tp3.repo;

import org.springframework.data.repository.CrudRepository;

import tp3.model.producto.Gondola;

public interface GondolaRepository extends CrudRepository<Gondola, Long> {
  // tipo de dato y primary key
}
