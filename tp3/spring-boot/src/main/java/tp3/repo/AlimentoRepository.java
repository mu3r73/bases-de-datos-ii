package tp3.repo;

import org.springframework.data.repository.CrudRepository;

import tp3.model.producto.Alimento;

public interface AlimentoRepository extends CrudRepository<Alimento, Long> {
  // tipo de dato y primary key
}
