package tp3.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tp3.model.Proveedor;

@Repository
public interface ProveedorRepository extends CrudRepository<Proveedor, Long> {
  // tipo de dato y primary key
  
  List<Proveedor> findByDescripcion(String descripcion);
  // Spring genera automáticamente el código de la consulta
  
}
