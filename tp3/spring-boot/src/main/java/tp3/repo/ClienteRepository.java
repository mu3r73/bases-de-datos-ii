package tp3.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tp3.model.Cliente;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long> {
  // tipo de dato y primary key
  
  List<Cliente> findByCodigo(String codigo);
  // Spring genera automáticamente el código de la consulta
  
}
