package tp3.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tp3.model.producto.Frio;

@Repository
public interface FrioRepository extends CrudRepository<Frio, Long> {
  // tipo de dato y primary key
}
