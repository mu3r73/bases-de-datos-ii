package tp3.service;

import java.util.List;

import tp3.dto.DetalleDTO;
import tp3.model.Detalle;
import tp3.model.Factura;

public interface IDetalleService {
  
  public DetalleDTO create(DetalleDTO dDTO);
  
  public List<Detalle> findByFactura(Factura f);

}
