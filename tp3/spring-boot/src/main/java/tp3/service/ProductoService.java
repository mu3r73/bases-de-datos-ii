package tp3.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp3.model.producto.Producto;
import tp3.repo.ProductoRepository;

@Service
public class ProductoService implements IProductoService {

  @Autowired
  private ProductoRepository pr;

  @Override
  public Producto create(Producto p) {
    p = this.pr.save(p);
    return p;
  }

  @Override
  public Producto findById(long id) {
    return this.pr.findById(id).orElse(null);
  }

  @Override
  public List<Producto> findAll() {
    List<Producto> ps = new ArrayList<>();
    this.pr.findAll().forEach(p -> ps.add(p));;
    return ps;
  }
  
  @Override
  public Producto findByCodigo(String codigo) {
    List<Producto> ps = this.pr.findByCodigo(codigo);
    if (ps.size() > 0) {
      return ps.get(0);
    }
    return null;
  }

}
