package tp3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp3.model.producto.Congelado;
import tp3.repo.CongeladoRepository;

@Service
public class CongeladoService implements ICongeladoService {

  @Autowired
  private CongeladoRepository cr;

  @Override
  public Congelado create(Congelado c) {
    c = this.cr.save(c);
    return c;
  }

}
