package tp3.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp3.model.Proveedor;
import tp3.repo.ProveedorRepository;

@Service
public class ProveedorService implements IProveedorService {

  @Autowired
  private ProveedorRepository pr;

  @Override
  public Proveedor create(Proveedor p) {
    p = this.pr.save(p);
    return p;
  }

  @Override
  public List<Proveedor> findAll() {
    List<Proveedor> ps = new ArrayList<>();
    this.pr.findAll().forEach(p -> ps.add(p));;
    return ps;
  }

  @Override
  public Proveedor findByDesc(String desc) {
    List<Proveedor> ps = this.pr.findByDescripcion(desc);
    if (ps.size() > 0) {
      return ps.get(0);
    }
    return null;
  }
  
}
