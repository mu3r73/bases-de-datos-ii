package tp3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp3.model.producto.Alimento;
import tp3.repo.AlimentoRepository;

@Service
public class AlimentoService implements IAlimentoService {

  @Autowired
  private AlimentoRepository ar;

  @Override
  public Alimento create(Alimento a) {
    a = this.ar.save(a);
    return a;
  }

}
