package tp3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp3.model.producto.Frio;
import tp3.repo.FrioRepository;

@Service
public class FrioService implements IFrioService {

  @Autowired
  private FrioRepository fr;

  @Override
  public Frio create(Frio f) {
    f = this.fr.save(f);
    return f;
  }

}
