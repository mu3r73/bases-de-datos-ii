package tp3.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp3.dto.DetalleDTO;
import tp3.dto.FacturaDTO;
import tp3.model.Cliente;
import tp3.model.Detalle;
import tp3.model.Factura;
import tp3.model.producto.Producto;
import tp3.repo.ClienteRepository;
import tp3.repo.FacturaRepository;
import tp3.repo.ProductoRepository;

@Service
public class FacturaService implements IFacturaService {

  @Autowired
  private FacturaRepository fr;

  @Autowired
  private ClienteRepository cr;
  
  @Autowired
  private ProductoRepository pr;

  @Override
  public FacturaDTO create(FacturaDTO fDTO) {
    Cliente c = this.cr.findById(fDTO.getIdCliente()).orElse(null);
    Factura f = new Factura(c, fDTO.getNumero());
    this.addDetalles(f, fDTO.getDetalles());
    f = this.fr.save(f);
    return new FacturaDTO(f);
    // this.addDetalles(f, fDTO.getDetalles());
  }

  private Factura addDetalles(Factura f, Collection<DetalleDTO> detalles) {
    Collection<Detalle> ds = new ArrayList<>();
    for (DetalleDTO dDTO : detalles) {
      Producto p = this.pr.findById(dDTO.getIdProducto()).orElse(null);
      ds.add(new Detalle(f, p, dDTO.getCantidad(), p.getPrecio()));
    }
    f.setDetalles(ds);
    return f;
  }

  @Override
  public Factura findByNumero(int numero) {
    List<Factura> fs = this.fr.findByNumero(numero);
    if (fs.size() > 0) {
      return fs.get(0);
    }
    return null;
  }

}
