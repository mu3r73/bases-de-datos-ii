package tp3.service;

import java.util.List;

import tp3.model.Proveedor;

public interface IProveedorService {

  public Proveedor create(Proveedor p);
  
  public List<Proveedor> findAll();
  
  public Proveedor findByDesc(String desc);
  
}
