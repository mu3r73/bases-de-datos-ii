package tp3.service;

import java.util.List;

import tp3.model.producto.Producto;

public interface IProductoService {
  
  public Producto create(Producto p);
  
  public Producto findById(long id);
  
  public List<Producto> findAll();
  
  public Producto findByCodigo(String codigo);

}
