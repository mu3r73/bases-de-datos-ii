package tp3.service;

import java.util.List;

import tp3.dto.ClienteDTO;
import tp3.model.Cliente;

public interface IClienteService {
  
  public ClienteDTO create(ClienteDTO cDTO);

  public ClienteDTO findById(long id);

  public Cliente findByCodigo(String codigo);

  public List<ClienteDTO> findAll();
  
  public ClienteDTO update(ClienteDTO cDTO);
  
  public void delete(long id);

}
