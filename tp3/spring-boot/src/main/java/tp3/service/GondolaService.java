package tp3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp3.model.producto.Gondola;
import tp3.repo.GondolaRepository;

@Service
public class GondolaService implements IGondolaService {

  @Autowired
  private GondolaRepository gr;

  @Override
  public Gondola create(Gondola g) {
    g = this.gr.save(g);
    return g;
  }

}
