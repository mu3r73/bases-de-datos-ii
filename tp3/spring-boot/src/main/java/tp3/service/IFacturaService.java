package tp3.service;

import tp3.dto.FacturaDTO;
import tp3.model.Factura;

public interface IFacturaService {
  
  public FacturaDTO create(FacturaDTO fDTO);
  
  public Factura findByNumero(int numero);

}
