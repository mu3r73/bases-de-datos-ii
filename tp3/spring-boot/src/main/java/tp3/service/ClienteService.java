  package tp3.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp3.dto.ClienteDTO;
import tp3.model.Cliente;
import tp3.repo.ClienteRepository;

@Service
public class ClienteService implements IClienteService {

  @Autowired
  private ClienteRepository cr;
  
  @Override
  public ClienteDTO create(ClienteDTO cDTO) {
    Cliente c = new Cliente(cDTO.getCodigo(), cDTO.getApellido(), cDTO.getNombre(), cDTO.getNroCuenta());
    this.cr.save(c);
    return new ClienteDTO(c);
  }

  @Override
  public ClienteDTO findById(long id) {
    Cliente c = this.cr.findById(id).orElse(null);
    ClienteDTO cDTO = null;
    if (c != null) {
      cDTO = new ClienteDTO(c);
    }
    return cDTO;
  }

  @Override
  public List<ClienteDTO> findAll() {
    List<ClienteDTO> cs = new ArrayList<>();
    this.cr.findAll().forEach(c -> cs.add(new ClienteDTO(c)));;
    return cs;
  }
  
  @Override
  public Cliente findByCodigo(String codigo) {
    List<Cliente> cs = this.cr.findByCodigo(codigo);
    if (cs.size() > 0) {
      return cs.get(0);
    }
    return null;
  }

  @Override
  public ClienteDTO update(ClienteDTO cDTO) {
    Cliente c = this.cr.findById(cDTO.getId()).orElse(null);
    if (c != null) {
      // el código de cliente no es actualizable
      c.setApellido(cDTO.getApellido());
      c.setNombre(cDTO.getNombre());
      // ¿debería poder cambiar de cuenta?
      c = this.cr.save(c);
      return new ClienteDTO(c);
    }
    return null;
  }

  @Override
  public void delete(long id) {
    this.cr.deleteById(id);
  }

}
