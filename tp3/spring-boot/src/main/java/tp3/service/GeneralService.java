package tp3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp3.model.producto.General;
import tp3.repo.GeneralRepository;

@Service
public class GeneralService implements IGeneralService {

  @Autowired
  private GeneralRepository gr;

  @Override
  public General create(General g) {
    g = this.gr.save(g);
    return g;
  }

}
