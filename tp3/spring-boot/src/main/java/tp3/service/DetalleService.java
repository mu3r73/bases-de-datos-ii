package tp3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp3.dto.DetalleDTO;
import tp3.model.Detalle;
import tp3.model.Factura;
import tp3.model.producto.Producto;
import tp3.repo.DetalleRepository;
import tp3.repo.FacturaRepository;
import tp3.repo.ProductoRepository;

@Service
public class DetalleService implements IDetalleService {

  @Autowired
  private DetalleRepository dr;
  
  @Autowired
  private FacturaRepository fr;
  
  @Autowired
  private ProductoRepository prodr;

  @Override
  public DetalleDTO create(DetalleDTO dDTO) {
    Factura f = fr.findById(dDTO.getIdFactura()).orElse(null);
    Producto prod = prodr.findById(dDTO.getIdProducto()).orElse(null);
    Detalle d = new Detalle(f, prod, dDTO.getCantidad(), prod.getPrecio());
    d = this.dr.save(d);
    return new DetalleDTO(d);
  }

  @Override
  public List<Detalle> findByFactura(Factura f) {
    return this.dr.findByFactura(f);
  }

}
