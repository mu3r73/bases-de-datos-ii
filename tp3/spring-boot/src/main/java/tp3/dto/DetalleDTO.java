package tp3.dto;

import tp3.model.Detalle;

public class DetalleDTO {
  
  private long id;
  private long idFactura;
  private long idProducto;
  private int cantidad;

  // constructores
  
  public DetalleDTO(long idProducto, int cantidad) {
    super();
    this.idProducto = idProducto;
    this.cantidad = cantidad;
  }
  
  public DetalleDTO(Detalle d) {
    super();
    this.id = d.getId();
    this.idFactura = d.getFactura().getId();
    this.idProducto = d.getProducto().getId();
    this.cantidad = d.getCantidad();
  }
  
  // getters y setters
  
  public long getId() {
    return this.id;
  }
  
  public void setId(long id) {
    this.id = id;
  }

  public long getIdFactura() {
    return idFactura;
  }

  public void setIdFactura(long idFactura) {
    this.idFactura = idFactura;
  }

  public long getIdProducto() {
    return idProducto;
  }

  public void setIdProducto(long idProducto) {
    this.idProducto = idProducto;
  }

  public int getCantidad() {
    return cantidad;
  }

  public void setCantidad(int cantidad) {
    this.cantidad = cantidad;
  }

}
