package tp3.dto;

import tp3.model.Cliente;

public class ClienteDTO {

  private long id;
  private String codigo;
  private String apellido;
  private String nombre;  
  private String nroCuenta;
  private long idCuenta;
  
  // constructores
  
  public ClienteDTO(String codigo, String apellido, String nombre, String nroCuenta) {
    super();
    this.codigo = codigo;
    this.apellido = apellido;
    this.nombre = nombre;
    this.nroCuenta = nroCuenta;
  }
  
  public ClienteDTO(Cliente c) {
    this(c.getCodigo(), c.getApellido(), c.getNombre(), c.getCuenta().getNumero());
    this.id = c.getId();
    this.idCuenta = c.getCuenta().getId();
  }

  // getters y setters
  
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getApellido() {
    return apellido;
  }

  public void setApellido(String apellido) {
    this.apellido = apellido;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getNroCuenta() {
    return nroCuenta;
  }

  public void setNroCuenta(String nroCuenta) {
    this.nroCuenta = nroCuenta;
  }

  public long getIdCuenta() {
    return idCuenta;
  }

  public void setIdCuenta(long idCuenta) {
    this.idCuenta = idCuenta;
  }
    
}
