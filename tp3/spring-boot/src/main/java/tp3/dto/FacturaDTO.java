package tp3.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import tp3.model.Factura;

public class FacturaDTO {
  
  private long id;  
  private long idCliente;
  private LocalDateTime fecha;  
  private int numero;  
  private Collection<DetalleDTO> detalles = new ArrayList<>();
  
  // constructores
  
  public FacturaDTO(long idCliente, int numero, Collection<DetalleDTO> detalles) {
    super();
    this.idCliente = idCliente;
    this.fecha = LocalDateTime.now();
    this.numero = numero;
    this.detalles = detalles;
  }
  
  public FacturaDTO(Factura f) {
    super();
    this.id = f.getId();
    this.idCliente = f.getCliente().getId();
    this.fecha = f.getFecha();
    this.numero = f.getNumero();
    f.getDetalles().forEach(d -> this.detalles.add(new DetalleDTO(d)));
  }
  
  // getters y setters

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getIdCliente() {
    return idCliente;
  }

  public void setIdCliente(long clienteId) {
    this.idCliente = clienteId;
  }

  public LocalDateTime getFecha() {
    return fecha;
  }

  public void setFecha(LocalDateTime fecha) {
    this.fecha = fecha;
  }

  public int getNumero() {
    return numero;
  }

  public void setNumero(int numero) {
    this.numero = numero;
  }

  public Collection<DetalleDTO> getDetalles() {
    return detalles;
  }

  public void setDetalles(Collection<DetalleDTO> detalleDTOs) {
    this.detalles = detalleDTOs;
  }
  
}
