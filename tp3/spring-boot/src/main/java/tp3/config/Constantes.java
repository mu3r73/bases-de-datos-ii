package tp3.config;


public class Constantes {
  
  static private final boolean PRODUCTION = true;
  
  static private final String PROD_BASE_URL = "localhost:3306";
  static private final String PROD_USER = "admin";
  static private final String PROD_PASSWORD = "lamadrid";
  static private final String PROD_DB_NAME = "ciu-persistencia-tp-3";

  static private final String DEV_BASE_URL = "192.168.56.103:3306";
  static private final String DEV_USER = "muerte";
  static private final String DEV_PASSWORD = "gojira";
  static private final String DEV_DB_NAME = "tp3";
  
  static private final String DRIVER_CLS_NAME = "com.mysql.cj.jdbc.Driver";

  
  public static String getUrl() {
    return "jdbc:mysql://" + Constantes.getBaseUrl() + "/" + Constantes.getDBName();
  }
  
  private static String getBaseUrl() {
    return Constantes.PRODUCTION ? Constantes.PROD_BASE_URL : Constantes.DEV_BASE_URL;
  }
  
  private static String getDBName() {
    return Constantes.PRODUCTION ? Constantes.PROD_DB_NAME : Constantes.DEV_DB_NAME;
  }

  public static String getUser() {
    return Constantes.PRODUCTION ? Constantes.PROD_USER : Constantes.DEV_USER;
  }
  
  public static String getPassword() {
    return Constantes.PRODUCTION ? Constantes.PROD_PASSWORD : Constantes.DEV_PASSWORD;
  }
  
  public static String getDriverClsName() {
    return Constantes.DRIVER_CLS_NAME;
  }
    
}
