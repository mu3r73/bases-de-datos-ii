package tp3.config;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfig {

  @Bean
  public DataSource getDataSource() {
    DataSourceBuilder<?> dsb = DataSourceBuilder.create();
    dsb.driverClassName(Constantes.getDriverClsName())
        .url(Constantes.getUrl() + "?allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=GMT")
        .username(Constantes.getUser())
        .password(Constantes.getPassword());
    return dsb.build();
  }

}
