package tp2b_model.producto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import tp2b_model.Proveedor;


@Entity
@Table(name = "frio")
public class Frio extends Alimento {

  @Column(name = "temperatura_minima", updatable = false)
  private Integer temperaturaMinima;
  
  @Column(name = "temperatura_maxima", updatable = false)
  private Integer temperaturaMaxima;

  // constructores
  public Frio(String codigo, String descripcion, Proveedor proveedor, Optional<BigDecimal> monto,
      Integer temperaturaMinima, Integer temperaturaMaxima) {
    super(codigo, descripcion, proveedor, monto);
    this.temperaturaMinima = temperaturaMinima;
    this.temperaturaMaxima = temperaturaMaxima;
  }

  // getters y setters
  public Integer getTemperaturaMinima() {
    return temperaturaMinima;
  }

  public void setTemperaturaMinima(Integer temperaturaMinima) {
    this.temperaturaMinima = temperaturaMinima;
  }

  public Integer getTemperaturaMaxima() {
    return temperaturaMaxima;
  }

  public void setTemperaturaMaxima(Integer temperaturaMaxima) {
    this.temperaturaMaxima = temperaturaMaxima;
  }

  // overrides
  @Override
  public BigDecimal getPrecioFinal() {
    BigDecimal monto = this.getPrecio().getMonto();
    BigDecimal incremento = monto.multiply(BigDecimal.valueOf(5)).divide(BigDecimal.valueOf(100));
    BigDecimal descuento = (ChronoUnit.DAYS.between(this.getFechaDeIngreso(), LocalDate.now()) > 5)
        ? monto.multiply(BigDecimal.valueOf(50)).divide(BigDecimal.valueOf(100))
        : BigDecimal.valueOf(0);
    return monto.add(incremento).subtract(descuento);
  }
  
}
