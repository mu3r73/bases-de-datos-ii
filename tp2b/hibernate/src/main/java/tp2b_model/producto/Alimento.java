package tp2b_model.producto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import tp2b_model.Proveedor;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "alimento")
public abstract class Alimento extends Producto {

  @Column(name = "fecha_de_ingreso", nullable = false, updatable = false)
  private LocalDate fechaDeIngreso;
  
  // constructores
  public Alimento(String codigo, String descripcion, Proveedor proveedor, Optional<BigDecimal> monto) {
    super(codigo, descripcion, proveedor, monto);
    this.fechaDeIngreso = LocalDate.now();
  }
  
  // getters y setters
  public LocalDate getFechaDeIngreso() {
    return fechaDeIngreso;
  }

  public void setFechaDeIngreso(LocalDate fechaDeIngreso) {
    this.fechaDeIngreso = fechaDeIngreso;
  }
  
}
