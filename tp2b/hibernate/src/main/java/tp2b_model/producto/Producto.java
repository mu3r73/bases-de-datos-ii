package tp2b_model.producto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import tp2b_model.Precio;
import tp2b_model.Proveedor;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "producto")
public abstract class Producto {
  
  @Id
  @GeneratedValue
  private long id;  

  @Column(nullable = false, unique = true, updatable = false)
  private String codigo;  

  @Column(nullable = false)
  private String descripcion;  

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "fk_id_precio", referencedColumnName = "id")
  private Precio precio;  

  @ManyToMany
  @JoinTable(
    name = "producto_proveedor",
    joinColumns = {
      @JoinColumn(name = "fk_id_producto", referencedColumnName = "id", insertable = false, updatable = false)
    },
    inverseJoinColumns = {
      @JoinColumn(name = "fk_id_proveedor", referencedColumnName = "id", insertable = false, updatable = false)
    }
  )
  private Collection<Proveedor> proveedores = new ArrayList<>();

  // constructores
  public Producto() {
    super();
  }

  public Producto(String codigo, String descripcion, Proveedor proveedor, Optional<BigDecimal> monto) {
    super();
    this.codigo = codigo;
    this.descripcion = descripcion;
    this.proveedores.add(proveedor);
    monto.ifPresent(m -> { 
      Precio precio = new Precio(m, this);
      this.precio = precio;
    });
  }

  // overrides
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (!(obj instanceof Producto))
      return false;
    Producto other = (Producto) obj;
    return Objects.equals(id, other.id);
  }
  
  @Override
  public String toString() {
    String precio = (this.precio != null) ? ("$" + this.precio.getMonto().toString()) : "(no disponible)";
    return this.codigo + ": " + this.descripcion + " - precio: " + precio;
  }
  
  // getters y setters
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public Precio getPrecio() {
    return precio;
  }
  
  public void setPrecio(BigDecimal monto) {
    Precio precio = new Precio(monto, this);
    this.precio = precio;
  }

  public Collection<Proveedor> getProveedores() {
    return proveedores;
  }

  public void setProveedores(Collection<Proveedor> proveedores) {
    this.proveedores = proveedores;
  }
  
  public abstract BigDecimal getPrecioFinal();
  
}
