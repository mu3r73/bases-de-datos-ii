package tp2b_model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tp2b_model.producto.Producto;


@Entity
@Table(name = "precio")
public class Precio {
  
  @Id
  @GeneratedValue
  private long id;  

  @Column(nullable = false, updatable = false)
  private BigDecimal monto;  

  @Column(nullable = false, updatable = false)
  private LocalDateTime fecha;  

  @ManyToOne
  @JoinColumn(name = "fk_id_producto", referencedColumnName = "id")
  private Producto producto;
  
  // constructores
  public Precio() {
    super();
  }

  public Precio(BigDecimal monto, Producto producto) {
    super();
    this.monto = monto;
    this.fecha = LocalDateTime.now();
    this.producto = producto;
  }

  // overrides
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (!(obj instanceof Precio))
      return false;
    Precio other = (Precio) obj;
    return Objects.equals(id, other.id);
  }
  
  @Override
  public String toString() {
    return this.monto.toString() + " - " + this.fecha + " - prod: " + this.producto.getCodigo();
  }
    
  // getters y setters
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public BigDecimal getMonto() {
    return monto;
  }

  public void setMonto(BigDecimal monto) {
    this.monto = monto;
  }

  public LocalDateTime getFecha() {
    return fecha;
  }

  public void setFecha(LocalDateTime fecha) {
    this.fecha = fecha;
  }

  public Producto getProducto() {
    return producto;
  }

  public void setProducto(Producto producto) {
    this.producto = producto;
  }
  
}
