package tp2b_model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "cliente")
public class Cliente {

  @Id
  @GeneratedValue
  private long id;

  @Column(nullable = false, unique = true, updatable = false)
  private String codigo;
  
  @Column(nullable = false)
  private String apellido;
  
  @Column(nullable = false)
  private String nombre;  
  
  @OneToOne(mappedBy = "cliente", cascade = CascadeType.ALL)
  private Cuenta cuenta;  

  @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
  private Collection<Factura> facturas = new ArrayList<>();
  
  // constructores
  public Cliente() {
    super();
  }

  public Cliente(String codigo, String apellido, String nombre, String numeroDeCuenta) {
    super();
    this.codigo = codigo;
    this.apellido = apellido;
    this.nombre = nombre;
    this.cuenta = new Cuenta(numeroDeCuenta, this);
  }

  // overrides
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (!(obj instanceof Cliente))
      return false;
    Cliente other = (Cliente) obj;
    return Objects.equals(id, other.id);
  }
  
  @Override
  public String toString() {
    return this.codigo + ": " + this.apellido + ", " + this.nombre + " - cuenta " + this.cuenta.getNumero();
  }

  // getters y setters
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getApellido() {
    return apellido;
  }

  public void setApellido(String apellido) {
    this.apellido = apellido;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public Cuenta getCuenta() {
    return cuenta;
  }

  public void setCuenta(Cuenta cuenta) {
    this.cuenta = cuenta;
  }

  public Collection<Factura> getFacturas() {
    return facturas;
  }

  public void setFacturas(Collection<Factura> facturas) {
    this.facturas = facturas;
  }

}
