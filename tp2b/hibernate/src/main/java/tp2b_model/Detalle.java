package tp2b_model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tp2b_model.producto.Producto;


@Entity
@Table(name = "detalle")
public class Detalle {
  
  @Id
  @GeneratedValue
  private long id;  

  @ManyToOne
  @JoinColumn(name = "fk_id_factura", referencedColumnName = "id")
  private Factura factura;  

  @ManyToOne
  @JoinColumn(name = "fk_id_producto")
  private Producto producto;  

  @Column(nullable = false)
  private Integer cantidad;  

  @ManyToOne
  @JoinColumn(name = "fk_id_precio")
  private Precio precio;
  
  // constructores
  public Detalle() {
    super();
  }

  public Detalle(Factura factura, Producto producto, Integer cantidad, Precio precio) {
    super();
    this.factura = factura;
    this.producto = producto;
    this.cantidad = cantidad;
    this.precio = precio;
  }

  // overrides
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (!(obj instanceof Detalle))
      return false;
    Detalle other = (Detalle) obj;
    return Objects.equals(id, other.id);
  }

  // getters y setters
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Factura getFactura() {
    return factura;
  }

  public void setFactura(Factura factura) {
    this.factura = factura;
  }

  public Producto getProducto() {
    return producto;
  }

  public void setProducto(Producto producto) {
    this.producto = producto;
  }

  public Integer getCantidad() {
    return cantidad;
  }

  public void setCantidad(Integer cantidad) {
    this.cantidad = cantidad;
  }

  public Precio getPrecio() {
    return precio;
  }

  public void setPrecio(Precio precio) {
    this.precio = precio;
  }
  
}
