package tp2b_store;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import tp2b_config.HibernateUtils;
import tp2b_model.Proveedor;
import tp2b_model.producto.Alimento;
import tp2b_model.producto.Congelado;
import tp2b_model.producto.Frio;
import tp2b_model.producto.General;
import tp2b_model.producto.Gondola;
import tp2b_model.producto.Producto;


public class TP2bStore {
  
  private Session session;

  /**
   * sesión
   */
  
  public void startSession(String hbm2ddl) {
    HibernateUtils.buildSessionFactory(hbm2ddl);
    this.session = HibernateUtils.getSessionFactory().openSession();
  }
  
  public void closeSession() {
    this.session.close();
    HibernateUtils.closeSessionFactory();
  }
  
  public boolean isSessionOpen() {
    return this.session.isOpen();
  }

  /**
   *  altas
   */
  
  public void addProveedores(List<Proveedor> ps) {
    Transaction t = null;

    try {
      System.out.println(">>> store: alta de proveedores");
      t = this.session.beginTransaction();

      for (Proveedor p : ps) {
        System.out.println(">>> store: agregando proveedor: " + p);
        this.session.persist(p);
      }
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace(); 
    }
  }
  
  public void addFrios(List<Frio> fs) {
    Transaction t = null;

    try {
      System.out.println(">>> store: alta de alimentos fríos");
      t = this.session.beginTransaction();
      
      for (Frio f : fs) {
        System.out.println(">>> store: agregando alimento frío: " + f);
        this.session.persist(f);
      }
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace();
    }
  }
  
  public void addCongelados(List<Congelado> cs) {
    Transaction t = null;

    try {
      System.out.println(">>> store: alta de alimentos congelados");
      t = this.session.beginTransaction();
      
      for (Congelado c : cs) {
        System.out.println(">>> store: agregando alimento congelado: " + c);
        this.session.persist(c);
      }
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace();
    }
  }
  
  public void addGondolas(List<Gondola> gs) {
    Transaction t = null;

    try {
      System.out.println(">>> store: alta de alimentos de góndola");
      t = this.session.beginTransaction();
      
      for (Gondola g : gs) {
        System.out.println(">>> store: agregando alimento de góndola: " + g);
        this.session.persist(g);
      }
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace();
    }
  }
  
  public void addGenerales(List<General> gs) {
    Transaction t = null;

    try {
      System.out.println(">>> store: alta de productos generales");
      t = this.session.beginTransaction();
      
      for (General g : gs) {
        System.out.println(">>> store: agregando producto general: " + g);
        this.session.persist(g);
      }
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace();
    }
  }
  
  /**
   *  consultas
   */
  
  public Proveedor fetchProveedor(String desc) {
    Query q = this.session.createQuery("from Proveedor where descripcion = :desc");
    q.setParameter("desc", desc);
    return (Proveedor) q.getResultList().get(0);
  }

  @SuppressWarnings("unchecked")
  public List<Producto> fetchProductos() {
    Query q = this.session.createQuery("from Producto");
    return q.getResultList();
  }

  @SuppressWarnings("unchecked")
  public List<Alimento> fetchAlimentos() {
    Query q = this.session.createQuery("from Alimento");
    return q.getResultList();
  }
  
  @SuppressWarnings("unchecked")
  public List<Gondola> fetchGondola() {
    Query q = this.session.createQuery("from Gondola");
    return q.getResultList();
  }
  
  @SuppressWarnings("unchecked")
  public List<General> fetchGeneralesPesoMinPrecioFinalMin(Integer pesoMin, BigDecimal precioMin) {
    Query q = this.session.createQuery("from General where peso > :pesoMin");
    q.setParameter("pesoMin", pesoMin);
    return (List<General>) q.getResultList()
        .stream()
        .filter(g -> ((General) g).getPrecioFinal().compareTo(precioMin) == 1)
        .collect(Collectors.toList());
  }

}
