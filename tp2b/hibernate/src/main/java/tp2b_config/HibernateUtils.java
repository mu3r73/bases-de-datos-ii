package tp2b_config;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;


public class HibernateUtils {

  private static SessionFactory sf;
  
  public static void buildSessionFactory(String hbm2ddl) {
    Properties props = new Properties();
    props.put("hibernate.connection.url", Constantes.getUrl());
    props.put("hibernate.connection.username", Constantes.getUser());
    props.put("hibernate.connection.password", Constantes.getPassword());
    props.put("hibernate.hbm2ddl.auto", hbm2ddl);
    
    ServiceRegistry sr = new StandardServiceRegistryBuilder()
        .configure()
        .applySettings(props)
        .build();
    
    HibernateUtils.sf = new MetadataSources(sr).buildMetadata().buildSessionFactory();
  }
  
  public static SessionFactory getSessionFactory() {
    return HibernateUtils.sf;
  }
  
  public static void closeSessionFactory() {
    HibernateUtils.sf.close();
  }
  
}
