package tp2b;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tp2b_model.Proveedor;
import tp2b_model.producto.Congelado;
import tp2b_model.producto.Frio;
import tp2b_model.producto.General;
import tp2b_model.producto.Gondola;
import tp2b_model.producto.Producto;
import tp2b_store.TP2bStore;

public class TP2bEj2aTestConsultaProductos {

  private static Logger l;
  private static TP2bStore s;

  @BeforeClass
  public static void config() {
    l = LogManager.getLogger(TP2bEj2aTestConsultaProductos.class);
    s = new TP2bStore();
  }
  
  @Test
  public void consultaProductos() {
    l.info(">>> tp2bEj2a: inicialización");
    s.startSession("create");
    
    List<Proveedor> provs = TP2bTestData.getProveedores();
    s.addProveedores(provs);
    
    // carga de productos por tipo
    List<Frio> fs = TP2bTestData.getFrios();
    s.addFrios(fs);
    
    List<Congelado> cs = TP2bTestData.getCongelados();
    s.addCongelados(cs);
    
    List<Gondola> gonds = TP2bTestData.getGondolas();
    s.addGondolas(gonds);
    
    List<General> gens = TP2bTestData.getGenerales();
    s.addGenerales(gens);
    
    // resultado esperado = fríos + congelados + góndola + generales
    List<Producto> prods = new ArrayList<>();
    prods.addAll(fs);
    prods.addAll(cs);
    prods.addAll(gonds);
    prods.addAll(gens);
    // resultado de consulta
    List<Producto> prodsRes = s.fetchProductos();
    
    // verifico que ambas listas tengan el mismo tamaño
    Assert.assertEquals(prods.size(), prodsRes.size());
    
    // verifico que cada producto en prods esté en prodsRes
    for (Producto p : prods) {
      Assert.assertTrue(prodsRes.contains(p));
    }

    // verifico que cada producto en prodsRes esté en prods
    for (Producto p : prodsRes) {
      Assert.assertTrue(prods.contains(p));
    }
    
    s.closeSession();
    Assert.assertFalse(s.isSessionOpen());    
  }

  @AfterClass
  public static void close() {
    l.info(">>> tp2bEj2a: terminado");
  }
  
}
