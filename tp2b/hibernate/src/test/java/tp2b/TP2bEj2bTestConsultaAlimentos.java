package tp2b;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tp2b_model.Proveedor;
import tp2b_model.producto.Alimento;
import tp2b_model.producto.Congelado;
import tp2b_model.producto.Frio;
import tp2b_model.producto.General;
import tp2b_model.producto.Gondola;
import tp2b_store.TP2bStore;

public class TP2bEj2bTestConsultaAlimentos {
  
  private static Logger l;
  private static TP2bStore s;

  @BeforeClass
  public static void config() {
    l = LogManager.getLogger(TP2bEj2bTestConsultaAlimentos.class);
    s = new TP2bStore();
  }
  
  @Test
  public void consultaAlimentos() {
    l.info(">>> tp2bEj2b: inicialización");
    s.startSession("create");
    
    List<Proveedor> provs = TP2bTestData.getProveedores();
    s.addProveedores(provs);
    
    // carga de productos por tipo
    List<Frio> fs = TP2bTestData.getFrios();
    s.addFrios(fs);
    
    List<Congelado> cs = TP2bTestData.getCongelados();
    s.addCongelados(cs);
    
    List<Gondola> gonds = TP2bTestData.getGondolas();
    s.addGondolas(gonds);
    
    List<General> gens = TP2bTestData.getGenerales();
    s.addGenerales(gens);
    
    // resultado esperado = fríos + congelados + góndola
    List<Alimento> as = new ArrayList<>();
    as.addAll(fs);
    as.addAll(cs);
    as.addAll(gonds);
    // resultado de consulta
    List<Alimento> asRes = s.fetchAlimentos();
    
    // verifico que ambas listas tengan el mismo tamaño
    Assert.assertEquals(as.size(), asRes.size());
    
    // verifico que cada aliemnto en as esté en asRes
    for (Alimento a : as) {
      Assert.assertTrue(asRes.contains(a));
    }

    // verifico que cada alimento en asRes esté en as
    for (Alimento a : asRes) {
      Assert.assertTrue(as.contains(a));
    }
    
    s.closeSession();
    Assert.assertFalse(s.isSessionOpen());    
  }
  
  @AfterClass
  public static void close() {
    l.info(">>> tp2bEj2b: terminado");
  }

}
