package tp2b;

import java.math.BigDecimal;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tp2b_model.Proveedor;
import tp2b_model.producto.Congelado;
import tp2b_model.producto.Frio;
import tp2b_model.producto.General;
import tp2b_model.producto.Gondola;
import tp2b_store.TP2bStore;

public class TP2bEj2dTestConsultaGeneralConCond {
  
  private static Logger l;
  private static TP2bStore s;

  @BeforeClass
  public static void config() {
    l = LogManager.getLogger(TP2bEj2dTestConsultaGeneralConCond.class);
    s = new TP2bStore();
  }
  
  @Test
  public void consultaGeneralConCondiciones() {
    l.info(">>> tp2bEj2d: inicialización");
    s.startSession("create");
    
    List<Proveedor> provs = TP2bTestData.getProveedores();
    s.addProveedores(provs);
    
    // carga de productos por tipo
    List<Frio> fs = TP2bTestData.getFrios();
    s.addFrios(fs);
    
    List<Congelado> cs = TP2bTestData.getCongelados();
    s.addCongelados(cs);
    
    List<Gondola> gonds = TP2bTestData.getGondolas();
    s.addGondolas(gonds);
    
    List<General> gens = TP2bTestData.getGenerales();
    s.addGenerales(gens);
    
    // resultado esperado = 2 productos generales, códigos P010 y P011
    
    // resultado de consulta
    List<General> genRes = s.fetchGeneralesPesoMinPrecioFinalMin(400, BigDecimal.valueOf(1500));
    
    // verifico que genRes tenga 2 resultados
    Assert.assertEquals(genRes.size(), 2);
    
    // verifico que genRes contenga al productos P010
    Assert.assertTrue(genRes.stream().anyMatch(g -> g.getCodigo().equals("P010")));
    
    // verifico que genRes contenga al productos P011
    Assert.assertTrue(genRes.stream().anyMatch(g -> g.getCodigo().equals("P011")));
    
    s.closeSession();
    Assert.assertFalse(s.isSessionOpen());    
  }

  
  @AfterClass
  public static void close() {
    l.info(">>> tp2bEj2d: terminado");
  }

}
