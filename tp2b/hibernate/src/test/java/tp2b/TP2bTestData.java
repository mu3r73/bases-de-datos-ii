package tp2b;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import tp2b_model.Proveedor;
import tp2b_model.producto.Congelado;
import tp2b_model.producto.Frio;
import tp2b_model.producto.General;
import tp2b_model.producto.Gondola;
import tp2b_store.TP2bStore;

public class TP2bTestData {
  
  private static TP2bStore s = new TP2bStore();

  /**
   * datos de prueba
   */
  
  public static List<Proveedor> getProveedores() {
    List<Proveedor> ps = new ArrayList<>();
    ps.add(new Proveedor("Mayorista Urris"));
    ps.add(new Proveedor("Carcor S.R.L."));
    ps.add(new Proveedor("Hadén S.A."));
    ps.add(new Proveedor("Quetmata Mayorista"));
    return ps;
  }
  
  public static List<Frio> getFrios() {
    s.startSession("none");
    
    List<Frio> fs = new ArrayList<>();
    
    fs.add(new Frio("P001", "Manteca 200g", 
                          s.fetchProveedor("Quetmata Mayorista"),
                          Optional.of(BigDecimal.valueOf(125)), 2, 4)); 
        
    fs.add(new Frio("P002", "Yogur bebible 1l",
                          s.fetchProveedor("Quetmata Mayorista"), 
                          Optional.of(BigDecimal.valueOf(87)), 2, 8));
    
    s.closeSession();
    
    return fs;
  }
  
  public static List<Congelado> getCongelados() {
    s.startSession("none");
    
    List<Congelado> cs = new ArrayList<>();
    
    cs.add(new Congelado("P003", "Bocaditos de carne 380g",
                           s.fetchProveedor("Carcor S.R.L."),
                           Optional.of(BigDecimal.valueOf(173)))); 
        
    cs.add(new Congelado("P004", "Mix de frutos del bosque congelados 1kg",
                           s.fetchProveedor("Carcor S.R.L."),
                           Optional.of(BigDecimal.valueOf(203))));
    
    s.closeSession();
    
    return cs;
  }

  public static List<Gondola> getGondolas() {
    s.startSession("none");
    
    List<Gondola> gs = new ArrayList<>();
    
    gs.add(new Gondola("P005", "Aceite de girasol 1.5l",
                          s.fetchProveedor("Mayorista Urris"),
                          Optional.of(BigDecimal.valueOf(148)), 1500)); 
        
    gs.add(new Gondola("P006", "Harina 0000 1kg",
                          s.fetchProveedor("Mayorista Urris"),
                          Optional.of(BigDecimal.valueOf(48)), 1000));
    
    s.closeSession();
    
    return gs;
  }
  
  public static List<General> getGenerales() {
    s.startSession("none");
    
    List<General> gs = new ArrayList<>();
    
    gs.add(new General("P007", "Secador de goma",
                          s.fetchProveedor("Hadén S.A."),
                          Optional.of(BigDecimal.valueOf(46)), 170)); 
        
    gs.add(new General("P008", "Quitamanchas 500cc",
                          s.fetchProveedor("Hadén S.A."),
                          Optional.of(BigDecimal.valueOf(70)), 550));
    
    gs.add(new General("P009", "Mochila escolar 25.3l",
                          s.fetchProveedor("Hadén S.A."),
                          Optional.of(BigDecimal.valueOf(2390)), 300));
    
    gs.add(new General("P010", "Detergente en polvo 10 kg",
                          s.fetchProveedor("Hadén S.A."),
                          Optional.of(BigDecimal.valueOf(1657)), 10000));
    
    gs.add(new General("P011", "Vino Merlot importado de Italia cosecha 2005 750ml",
                          s.fetchProveedor("Hadén S.A."),
                          Optional.of(BigDecimal.valueOf(10304)), 1450));
    
    s.closeSession();
    
    return gs;
  }

}
