package tp2b;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tp2b_model.Proveedor;
import tp2b_model.producto.Congelado;
import tp2b_model.producto.Frio;
import tp2b_model.producto.General;
import tp2b_model.producto.Gondola;
import tp2b_store.TP2bStore;

public class TP2bEj2cTestConsultaGondola {

  private static Logger l;
  private static TP2bStore s;

  @BeforeClass
  public static void config() {
    l = LogManager.getLogger(TP2bEj2cTestConsultaGondola.class);
    s = new TP2bStore();
  }
  
  @Test
  public void consultaGondola() {
    l.info(">>> tp2bEj2c: inicialización");
    s.startSession("create");
    
    List<Proveedor> provs = TP2bTestData.getProveedores();
    s.addProveedores(provs);
    
    // carga de productos por tipo
    List<Frio> fs = TP2bTestData.getFrios();
    s.addFrios(fs);
    
    List<Congelado> cs = TP2bTestData.getCongelados();
    s.addCongelados(cs);
    
    List<Gondola> gonds = TP2bTestData.getGondolas();
    s.addGondolas(gonds);
    
    List<General> gens = TP2bTestData.getGenerales();
    s.addGenerales(gens);
    
    // resultado esperado = góndola (variable gonds)
    
    // resultado de consulta
    List<Gondola> gondsRes = s.fetchGondola();
    
    // verifico que ambas listas tengan el mismo tamaño
    Assert.assertEquals(gonds.size(), gondsRes.size());
    
    // verifico que cada aliemnto de góndola en gonds esté en gondsRes
    for (Gondola g : gonds) {
      Assert.assertTrue(gondsRes.contains(g));
    }

    // verifico que cada alimento de góndola en gondsRes esté en gonds
    for (Gondola g : gondsRes) {
      Assert.assertTrue(gonds.contains(g));
    }
    
    s.closeSession();
    Assert.assertFalse(s.isSessionOpen());    
  }
  
  @AfterClass
  public static void close() {
    l.info(">>> tp2bEj2c: terminado");
  }
  
}
