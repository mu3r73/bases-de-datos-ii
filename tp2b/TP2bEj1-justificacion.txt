El tipo de herencia hibernate que considero más conveniente para este
caso es InheritanceType.JOINED.


Justificación:

- Necesito relaciones y consultas polimórficas. Ejemplos de relación
polimórfica: de Precio a Producto, de Proveedor a Producto, de Detalle a
Producto. Ejemplos de consultas polimórficas: ejercicio 2-a) y 2-b).
Entonces tengo que descartar @MappedSuperclass:

  + @MappedSuperclass: la superclase mapeada no es una entidad, y no se
  mapea a una tabla en la base de datos. Por lo tanto, no permite
  definir relaciones polimórficas ni realizar consultas polimórficas.
  
  + Para los demás tipos de herencia, la superclase abstracta es una
  entidad. Esto permite definir relaciones polimórficas y realizar
  consultas polimórficas.


- Priorizo en primer lugar la consistencia y en segundo lugar la
performance:
  
  + Consistencia: Es importante poder marcar columnas como nullable =
  false (por ej., no quiero que haya alimentos sin fecha de ingreso, ni
  alimentos de góndola sin volumen, ni productos generales sin peso).
  Entonces, descarto InheritanceType.SINGLE_TABLE:
    
    * InheritanceType.SINGLE_TABLE mapea todas las subclases concretas a
    una única tabla. Esta tabla incluye todas las columnas de todas las
    subclases, y rellena con NULL las que no se usan en una instancia en
    particular. En las consultas polimórficas, Hibernate realiza un
    SELECT sobre la tabla única, por eso la performance es óptima. Sin
    embargo, no se pueden marcar columnas como nullable = false.
    
    * Los demás tipos de herencia son menos performantes que
    InheritanceType.SINGLE_TABLE, pero permiten marcar columnas como
    nullable = false, porque las subclases concretas se mapean a tablas
    separadas en la base de datos.

  + Performance: Cumplido el criterio de consistencia, prefiero el tipo
  de herencia que ofrece mejor performance. Entonces, descarto
  InheritanceType.TABLE_PER_CLASS:

    * InheritanceType.TABLE_PER_CLASS: una superclase abstracta es una
    entidad, pero no se mapea a una tabla en la base de datos. En las
    consultas polimórficas, Hibernate realiza un SELECT con UNION de las
    tablas que representan a las subclases concretas. Dependiendo de la
    cantidad de registros en las tablas, esta consulta puede traer
    problemas de performance. Especialmente si la jerarquía de clases se
    profundiza.

    * InheritanceType.JOINED: una superclase abstracta es una entidad, y
    además se mapea a una tabla en la base de datos. Esta tabla de la
    superclase abstracta contiene columnas para los atributos
    compartidos por todas las subclases. Las tablas de las subclases
    sólo tienen columnas para los atributos específicos, y tienen como
    clave principal una columna con el mismo valor que el registro
    correspondiente en la tabla de la superclase. En las consultas
    polimórficas, Hibernate realiza un SELECT con un JOIN de la tabla
    concreta y la(s) tabla(s) abstracta(s) necesaria(s) para recuperar
    todas las columnas requeridas. La complejidad de esta consulta es
    mayor que la de InheritanceType.SINGLE_TABLE, por eso su performance
    no es tan buena. Por otro lado, es menos compleja que la de
    InheritanceType.TABLE_PER_CLASS, por eso su performance es
    aceptable.


(
Fuente consultada:
https://thoughts-on-java.org/complete-guide-inheritance-strategies-jpa-hibernate
)
