use tpventas;

-- ejercicio 10:

-- para optimizar las consultas anteriores, debería crear índices para los foreign keys,
-- pero mysql ya lo hace automáticamente

-- por ejemplo:

-- para el ejercicio 4, índices para:

-- id_precio (foreign key) en la tabla producto:
-- create index indice_precio on producto(id_precio);

-- id_producto (foreign key) en la tabla precio:
-- create index indice_producto on precio(id_producto);
