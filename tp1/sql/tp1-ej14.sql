use tpventas;

-- stored procedures

-- insert de un registro factura
delimiter $$
create procedure nueva_factura(
  in p_id_cliente int,
  in p_fecha datetime,
  in p_numero int,
  out id_out int
)
begin
  declare exit handler for sqlexception rollback;
  start transaction;
    insert into factura (id_cliente, fecha, numero) values (p_id_cliente, p_fecha, p_numero);
    set id_out = last_insert_id();
  commit;
end $$

-- insert de un registro factura_producto
delimiter $$
create procedure nuevo_factura_producto(
  in p_id_factura int,
  in p_id_producto int,
  in p_cantidad int
)
begin
  declare exit handler for sqlexception rollback;
  start transaction;
    insert into factura_producto (id_factura, id_producto, cantidad) values (p_id_factura, p_id_producto, p_cantidad);
  commit;
end $$
