use tpventas;

-- ejercicio 5:
select codigo, descripcion, sum(cantidad) as cant_vendida
  from producto inner join factura_producto
  on producto.id_producto = factura_producto.id_producto
  group by producto.id_producto
  order by cant_vendida desc;
