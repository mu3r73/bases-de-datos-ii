use tpventas;

-- consulta:
select * from vista_compras where apellido like 'Pe%';

-- oracle mysql no permite indexar sobre una columna de un view
-- por ejemplo, esto no se puede hacer:
-- create index apellido on vista_compras(apellido);

-- indexando sobre la columna de la tabla subyacente:
create index indice_apellido on cliente(apellido);

-- al reiterar la consulta:
select * from vista_compras where apellido like 'Pe%';
-- ... no se percibe diferencia - mysql no usa el índice
-- (el plan de ejecución muestra un 'full table scan')

-- mysql sí usa el índice creado cuando se consulta sobre la tabla:
select * from cliente where apellido like 'Pe%';
-- (el plan de ejecución muestra un 'index range scan')
