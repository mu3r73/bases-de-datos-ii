-- base de datos
create schema tpventas
  default character set utf8
    collate utf8_spanish_ci;

use tpventas;

-- ejercicio 1: tablas
create table cliente (
  id_cliente int auto_increment not null primary key,
  codigo varchar(16) not null unique,
  apellido varchar(256) not null,
  nombre varchar(256) not null
);

create table factura (
  id_factura int auto_increment not null primary key,
  id_cliente int not null,
  fecha datetime not null,
  numero int unique not null,
  foreign key (id_cliente) references cliente(id_cliente)
);

create table producto (
  id_producto int auto_increment not null primary key,
  codigo varchar(16) not null unique,
  descripcion varchar(256) not null,
  id_precio int not null
);

create table precio (
  id_precio int auto_increment not null primary key,
  monto decimal(8, 2) not null,
  fecha date not null,
  id_producto int not null,
  foreign key (id_producto) references producto(id_producto)
);

create table factura_producto (
  id_factura int not null,
  id_producto int not null,
  cantidad int not null,
  primary key (id_factura, id_producto),
  foreign key (id_factura) references factura(id_factura),
  foreign key (id_producto) references producto(id_producto)
);

-- ... sigue después del ejercicio 2

-- ejercicio 2: datos de prueba
-- clientes
insert into cliente (codigo, apellido, nombre) values('C001', 'Sánchez', 'Franco');
insert into cliente (codigo, apellido, nombre) values('C002', 'Merlo', 'Bernardo');
insert into cliente (codigo, apellido, nombre) values('C003', 'Barri', 'Renato');
insert into cliente (codigo, apellido, nombre) values('C004', 'Pellegrini', 'Silvana');
insert into cliente (codigo, apellido, nombre) values('C005', 'Rodríguez', 'Nancy');
insert into cliente (codigo, apellido, nombre) values('C006', 'Benítez', 'Laura');
insert into cliente (codigo, apellido, nombre) values('C007', 'Martínez', 'Eduardo');
insert into cliente (codigo, apellido, nombre) values('C008', 'Heredia', 'Matilde');

-- productos
insert into producto (codigo, descripcion, id_precio) values ('P001', 'Taladro Percutor 650W', 1);
insert into producto (codigo, descripcion, id_precio) values ('P002', 'Termofusora 800W', 2);
insert into producto (codigo, descripcion, id_precio) values ('P003', 'Taladro Atornillador Eléctrico', 3);
insert into producto (codigo, descripcion, id_precio) values ('P004', 'Sierra Circular 1200W 71/4" 184mm Ángulo 45 A 90', 4);
insert into producto (codigo, descripcion, id_precio) values ('P005', 'Pistola Pintar Soplete Por Gravedad', 5);
insert into producto (codigo, descripcion, id_precio) values ('P006', 'Amoladora Angular 650W', 6);
insert into producto (codigo, descripcion, id_precio) values ('P007', 'Aerógrafo Profesional Metálico Con Compresor', 7);
insert into producto (codigo, descripcion, id_precio) values ('P008', 'Kit Llave Para Caño 3/4+1 +1.1/2 Stilson', 8);
insert into producto (codigo, descripcion, id_precio) values ('P009', 'Soldadora Inverter 120A', 9);
insert into producto (codigo, descripcion, id_precio) values ('P010', 'Desmalezadora Bordeadora A Nafta Explosion 52cc 3hp', 10);
insert into producto (codigo, descripcion, id_precio) values ('P011', 'Hidrolavadora 110bar 1400W', 11);
insert into producto (codigo, descripcion, id_precio) values ('P012', 'Sopladora Aspirador De Hojas', 12);
insert into producto (codigo, descripcion, id_precio) values ('P013', 'Válvula Regulador Dual Argón CO2 Caudalímetro Y Calentador', 13);
insert into producto (codigo, descripcion, id_precio) values ('P014', 'Amoladora Recta Neumática', 14);
insert into producto (codigo, descripcion, id_precio) values ('P015', 'Nivelador Separador 750 Arcos+150 Cuñas+1 Pinza', 15);
insert into producto (codigo, descripcion, id_precio) values ('P016', 'Pestañadora Excéntrica Refrigeración Con Embrague', 16);
insert into producto (codigo, descripcion, id_precio) values ('P017', 'Lápiz Grabador Eléctrico', 17);
insert into producto (codigo, descripcion, id_precio) values ('P018', 'Set Multiherramientas 110 Piezas', 18);

-- precios actuales
insert into precio (monto, fecha, id_producto) values (1499, '2019-04-01', 1);
insert into precio (monto, fecha, id_producto) values (1249, '2019-04-01', 2);
insert into precio (monto, fecha, id_producto) values (3550, '2019-04-01', 3);
insert into precio (monto, fecha, id_producto) values (3587, '2019-04-01', 4);
insert into precio (monto, fecha, id_producto) values (1625, '2019-04-01', 5);
insert into precio (monto, fecha, id_producto) values (1999, '2019-04-01', 6);
insert into precio (monto, fecha, id_producto) values (3359, '2019-04-01', 7);
insert into precio (monto, fecha, id_producto) values (2699, '2019-04-01', 8);
insert into precio (monto, fecha, id_producto) values (4099, '2019-04-01', 9);
insert into precio (monto, fecha, id_producto) values (3799, '2019-04-01', 10);
insert into precio (monto, fecha, id_producto) values (3078.41, '2019-04-01', 11);
insert into precio (monto, fecha, id_producto) values (2598, '2019-04-01', 12);
insert into precio (monto, fecha, id_producto) values (2099, '2019-04-01', 13);
insert into precio (monto, fecha, id_producto) values (1600, '2019-04-01', 14);
insert into precio (monto, fecha, id_producto) values (1500, '2019-04-01', 15);
insert into precio (monto, fecha, id_producto) values (2134.65, '2019-04-01', 16);
insert into precio (monto, fecha, id_producto) values (1517, '2019-04-01', 17);
insert into precio (monto, fecha, id_producto) values (5999, '2019-04-01', 18);

-- precios anteriores
insert into precio (monto, fecha, id_producto) values (1409.06, '2019-03-01', 1);
insert into precio (monto, fecha, id_producto) values (1124.10, '2019-03-01', 2);
insert into precio (monto, fecha, id_producto) values (1022.93, '2019-02-01', 2);
insert into precio (monto, fecha, id_producto) values (992.24, '2019-01-01', 2);
insert into precio (monto, fecha, id_producto) values (3337, '2019-03-01', 3);
insert into precio (monto, fecha, id_producto) values (1462.50, '2019-03-01', 5);
insert into precio (monto, fecha, id_producto) values (1859.07, '2019-03-01', 6);
insert into precio (monto, fecha, id_producto) values (1710.34, '2019-02-01', 6);
insert into precio (monto, fecha, id_producto) values (1693.23, '2019-01-01', 6);
insert into precio (monto, fecha, id_producto) values (2456.09, '2019-03-01', 8);
insert into precio (monto, fecha, id_producto) values (2284.16, '2019-02-01', 8);
insert into precio (monto, fecha, id_producto) values (2169.95, '2019-01-01', 8);
insert into precio (monto, fecha, id_producto) values (3647.04, '2019-03-01', 10);
insert into precio (monto, fecha, id_producto) values (3574.09, '2019-02-01', 10);
insert into precio (monto, fecha, id_producto) values (3359.64, '2019-01-01', 10);
insert into precio (monto, fecha, id_producto) values (2986.05, '2019-03-01', 11);
insert into precio (monto, fecha, id_producto) values (2468.10, '2019-03-01', 12);
insert into precio (monto, fecha, id_producto) values (1584, '2019-03-01', 14);
insert into precio (monto, fecha, id_producto) values (1568.16, '2019-02-01',14);
insert into precio (monto, fecha, id_producto) values (1536.79, '2019-01-01', 14);
insert into precio (monto, fecha, id_producto) values (1440, '2019-03-01', 15);
insert into precio (monto, fecha, id_producto) values (2113.30, '2019-03-01', 16);
insert into precio (monto, fecha, id_producto) values (1456.32, '2019-03-01', 17);

-- facturas
insert into factura (fecha, numero, id_cliente) values ('2019-04-01', 1, 1);
insert into factura (fecha, numero, id_cliente) values ('2019-04-01', 2, 4);
insert into factura (fecha, numero, id_cliente) values ('2019-04-03', 3, 2);
insert into factura (fecha, numero, id_cliente) values ('2019-04-04', 4, 3);
insert into factura (fecha, numero, id_cliente) values ('2019-04-06', 5, 3);
insert into factura (fecha, numero, id_cliente) values ('2019-04-07', 6, 6);
insert into factura (fecha, numero, id_cliente) values ('2019-04-11', 7, 3);
insert into factura (fecha, numero, id_cliente) values ('2019-04-11', 8, 1);
insert into factura (fecha, numero, id_cliente) values ('2019-04-12', 9, 4);
insert into factura (fecha, numero, id_cliente) values ('2019-04-12', 10, 4);
insert into factura (fecha, numero, id_cliente) values ('2019-04-17', 11, 3);
insert into factura (fecha, numero, id_cliente) values ('2019-04-19', 12, 5);
insert into factura (fecha, numero, id_cliente) values ('2019-04-19', 13, 1);
insert into factura (fecha, numero, id_cliente) values ('2019-04-20', 14, 2);
insert into factura (fecha, numero, id_cliente) values ('2019-04-21', 15, 4);
insert into factura (fecha, numero, id_cliente) values ('2019-04-24', 16, 6);
insert into factura (fecha, numero, id_cliente) values ('2019-04-26', 17, 1);
insert into factura (fecha, numero, id_cliente) values ('2019-04-29', 18, 2);

-- factura 1
insert into factura_producto (id_factura, cantidad, id_producto) values (1, 1, 8);
insert into factura_producto (id_factura, cantidad, id_producto) values (1, 1, 18);

-- factura 2
insert into factura_producto (id_factura, cantidad, id_producto) values (2, 1, 7);
insert into factura_producto (id_factura, cantidad, id_producto) values (2, 2, 2);

-- factura 3
insert into factura_producto (id_factura, cantidad, id_producto) values (3, 2, 11);

-- factura 4
insert into factura_producto (id_factura, cantidad, id_producto) values (4, 1, 15);
insert into factura_producto (id_factura, cantidad, id_producto) values (4, 1, 1);
insert into factura_producto (id_factura, cantidad, id_producto) values (4, 2, 16);
insert into factura_producto (id_factura, cantidad, id_producto) values (4, 1, 8);

-- factura 5
insert into factura_producto (id_factura, cantidad, id_producto) values (5, 1, 12);
insert into factura_producto (id_factura, cantidad, id_producto) values (5, 1, 9);
insert into factura_producto (id_factura, cantidad, id_producto) values (5, 1, 17);
insert into factura_producto (id_factura, cantidad, id_producto) values (5, 1, 16);

-- factura 6
insert into factura_producto (id_factura, cantidad, id_producto) values (6, 2, 16);

-- factura 7
insert into factura_producto (id_factura, cantidad, id_producto) values (7, 1, 9);
insert into factura_producto (id_factura, cantidad, id_producto) values (7, 2, 2);
insert into factura_producto (id_factura, cantidad, id_producto) values (7, 1, 5);
insert into factura_producto (id_factura, cantidad, id_producto) values (7, 2, 12);

-- factura 8
insert into factura_producto (id_factura, cantidad, id_producto) values (8, 1, 13);

-- factura 9
insert into factura_producto (id_factura, cantidad, id_producto) values (9, 2, 7);

-- factura 10
insert into factura_producto (id_factura, cantidad, id_producto) values (10, 1, 17);
insert into factura_producto (id_factura, cantidad, id_producto) values (10, 2, 8);
insert into factura_producto (id_factura, cantidad, id_producto) values (10, 1, 2);
insert into factura_producto (id_factura, cantidad, id_producto) values (10, 1, 9);
insert into factura_producto (id_factura, cantidad, id_producto) values (10, 2, 6);

-- factura 11
insert into factura_producto (id_factura, cantidad, id_producto) values (11, 1, 6);
insert into factura_producto (id_factura, cantidad, id_producto) values (11, 2, 1);

-- factura 12
insert into factura_producto (id_factura, cantidad, id_producto) values (12, 2, 13);
insert into factura_producto (id_factura, cantidad, id_producto) values (12, 2, 14);
insert into factura_producto (id_factura, cantidad, id_producto) values (12, 1, 17);
insert into factura_producto (id_factura, cantidad, id_producto) values (12, 2, 10);

-- factura 13
insert into factura_producto (id_factura, cantidad, id_producto) values (13, 1, 14);

-- factura 14
insert into factura_producto (id_factura, cantidad, id_producto) values (14, 2, 6);

-- factura 15
insert into factura_producto (id_factura, cantidad, id_producto) values (15, 2, 5);
insert into factura_producto (id_factura, cantidad, id_producto) values (15, 2, 12);

-- factura 16
insert into factura_producto (id_factura, cantidad, id_producto) values (16, 1, 18);
insert into factura_producto (id_factura, cantidad, id_producto) values (16, 2, 5);

-- factura 17
insert into factura_producto (id_factura, cantidad, id_producto) values (17, 1, 5);

-- factura 18
insert into factura_producto (id_factura, cantidad, id_producto) values (18, 2, 6);
insert into factura_producto (id_factura, cantidad, id_producto) values (18, 1, 9);
insert into factura_producto (id_factura, cantidad, id_producto) values (18, 1, 7);
insert into factura_producto (id_factura, cantidad, id_producto) values (18, 1, 17);

-- ... ejercicio 1 (cont.):
alter table producto
  add foreign key (id_precio) references precio(id_precio);
