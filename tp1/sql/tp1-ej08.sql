use tpventas;

-- ejercicio 8:
create or replace view vista_compras as
  select cliente.codigo as cod_cliente, apellido, nombre, numero, factura.fecha, producto.codigo as cod_producto, descripcion, monto as precio_unitario, cantidad, (monto * cantidad) as precio_total
  from cliente inner join factura inner join factura_producto inner join producto inner join precio
  on cliente.id_cliente = factura.id_cliente
  and factura.id_factura = factura_producto.id_factura
  and factura_producto.id_producto = producto.id_producto
  and producto.id_precio = precio.id_precio
  group by producto.id_producto, factura.id_factura, monto;
