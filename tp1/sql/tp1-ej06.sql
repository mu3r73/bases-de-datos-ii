use tpventas;

-- ejercicio 6:
select codigo, descripcion, sum(cantidad * monto) as monto_ventas
  from producto inner join factura_producto inner join precio
  on producto.id_producto = factura_producto.id_producto
    and producto.id_precio = precio.id_precio
  group by producto.id_producto
  order by monto_ventas desc;
