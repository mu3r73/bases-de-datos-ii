use tpventas;

-- ejercicio 15

-- agrego columna cant_facturas
alter table cliente
  add column cant_facturas int null;

-- agrego trigger
delimiter $$
create trigger after_insert_factura
after insert on factura for each row
begin
	declare cantidad int default 0;
	set cantidad = (select count(*) from factura where id_cliente = new.id_cliente);
	update cliente set cant_facturas = cantidad where id_cliente = new.id_cliente;
end $$

-- antes de agregar factura
select * from cliente where id_cliente = 2;

-- agrego factura
insert into factura (fecha, numero, id_cliente) values ('2019-05-01', 50, 2);

-- después de agregar factura
select * from cliente where id_cliente = 2;

-- para borrar el trigger
-- drop trigger after_insert_factura;
