use tpventas;

-- ejercicio 7:
select codigo, apellido, nombre, count(*) as cant_facturas
  from cliente inner join factura
    on cliente.id_cliente = factura.id_cliente
  group by cliente.id_cliente
  order by cant_facturas desc;
