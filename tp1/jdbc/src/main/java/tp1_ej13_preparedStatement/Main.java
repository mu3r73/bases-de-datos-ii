package tp1_ej13_preparedStatement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import tp1_config.Constantes;
import tp1_model.Precio;

public class Main {

  private static Connection dbConnection = null;

  private static int insertarPrecio(Precio precio) throws SQLException {
    String sqlPrecio = "insert into precio (monto, fecha, id_producto) values (?, ?, ?)";
    PreparedStatement ps = dbConnection.prepareStatement(sqlPrecio, 
        Statement.RETURN_GENERATED_KEYS);
    ps.setDouble(1, precio.getMonto());
    ps.setDate(2, new java.sql.Date(precio.getFecha().getTime()));
    ps.setInt(3, precio.getId_producto());
    ps.executeUpdate();
    
    ResultSet rs = ps.getGeneratedKeys();
    int idPrecio = -1;
    if (rs.next()) {
      idPrecio= rs.getInt(1);
      precio.setId_precio(idPrecio);
    }
    
    ps.close();
    return idPrecio;
  }

  private static void actualizarProducto(int idPrecio, Integer idProducto) throws SQLException {
    String sqlProducto = "update producto set id_precio = ? where id_producto = ?";
    PreparedStatement ps = dbConnection.prepareStatement(sqlProducto);
    ps.setInt(1, idPrecio);
    ps.setInt(2, idProducto);
    ps.executeUpdate();
    ps.close();
  }

  private static void mostrarProducto(Integer idProducto) throws SQLException {
    String query = "select codigo, descripcion, monto "
        + "from producto inner join precio "
        + "on producto.id_precio = precio.id_precio "
        + "where producto.id_producto = ?";
    PreparedStatement ps = dbConnection.prepareStatement(query);
    ps.setInt(1, idProducto);
    ResultSet rs = ps.executeQuery();
    System.out.println("código\tdescripción\tmonto");
    while (rs.next()) {
      System.out.println(rs.getString("codigo") + "\t" + rs.getString("descripcion") + "\t" +
          rs.getDouble("monto"));
    }
  }

  public static void main(String[] args) {
    
    try {
      // conexión
      Class.forName("com.mysql.cj.jdbc.Driver");
      dbConnection = DriverManager.getConnection("jdbc:mysql://" + Constantes.getUrl() + "/" + 
          Constantes.getDBName(), Constantes.getUser(), Constantes.getPassword());
      dbConnection.setAutoCommit(false);

      // insert precio + update producto
      Precio precio = new Precio(2038.98, new Date(), 6);
      
      int idPrecio = insertarPrecio(precio);
      actualizarProducto(idPrecio, precio.getId_producto());

      dbConnection.commit();
      
      // query
      mostrarProducto(precio.getId_producto());
      
      dbConnection.close();

    } catch (SQLException e) {
      e.printStackTrace();
    
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    
  }

}
