package tp1_ej14_callableStatement;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;

import tp1_config.Constantes;
import tp1_model.Factura;
import tp1_model.FacturaProducto;


public class Main {

  private static Connection dbConnection = null;
  
  private static double calcTotal(FacturaProducto[] fps) throws SQLException {
    double total = 0;
    String sqlSubtotal = "select monto * 3 from producto inner join precio on producto.id_precio = precio.id_precio where producto.id_producto = ?";
    for (FacturaProducto fp : fps) {
      PreparedStatement preparedStatement = dbConnection.prepareStatement(sqlSubtotal);
      preparedStatement.setInt(1, fp.getIdProducto());
      
      ResultSet rs = preparedStatement.executeQuery();
      if (rs.next()) {
        double subtotal = rs.getDouble(1);
        total += subtotal;
      }
    }
    return total;
  }

  private static int insertarFactura(Factura factura) throws SQLException {
    String query = "{call nueva_factura(?, ?, ?, ?)}";
    CallableStatement cs = dbConnection.prepareCall(query);
    cs.setInt(1, factura.getIdCliente());
    cs.setDate(2, new java.sql.Date(factura.getFecha().getTime()));
    cs.setInt(3, factura.getNumero());
    cs.registerOutParameter(4, Types.INTEGER);
    cs.execute();
    int idFactura = cs.getInt(4);
    cs.close();
    factura.setIdFactura(idFactura);
    return idFactura;
  }

  private static void insertarProductos(int idFactura, FacturaProducto[] fps) throws SQLException {
    String queryFP = "{call nuevo_factura_producto(?, ?, ?)}";
    CallableStatement cs = dbConnection.prepareCall(queryFP); 
    for (FacturaProducto fp : fps) {
      cs.setInt(1, idFactura);
      cs.setInt(2, fp.getIdProducto());
      cs.setInt(3, fp.getCantidad());
      cs.execute();
    }
    cs.close();
  }

  private static void mostrarFactura(int idFactura) throws SQLException {
    // encabezado
    String query = "select codigo, apellido, nombre, fecha, numero "
        + "from factura inner join cliente "
        + "on factura.id_cliente = cliente.id_cliente "
        + "where id_factura = ?";
    PreparedStatement preparedStatement = dbConnection.prepareStatement(query);
    preparedStatement.setInt(1, idFactura);
    
    ResultSet rs = preparedStatement.executeQuery();
    System.out.println("codigo\tcliente\tfecha\tnúmero");
    while (rs.next()) {
      System.out.println(rs.getString("codigo") + "\t" + rs.getString("apellido") + ", " +
          rs.getString("nombre") + "\t" + rs.getDate("fecha") + "\t" + rs.getInt("numero"));
    }
    // productos
    query = "select cantidad, codigo, descripcion, monto, cantidad * monto as subtotal "
        + "from factura_producto inner join producto inner join precio "
        + "on factura_producto.id_producto = producto.id_producto and producto.id_precio = precio.id_precio "
        + "where id_factura = ?";
    preparedStatement = dbConnection.prepareStatement(query);
    preparedStatement.setInt(1, idFactura);
    rs = preparedStatement.executeQuery();
    System.out.println("cantidad\tcódigo\tdescripción\tprecio unitario\tsubtotal");
    while (rs.next()) {
      System.out.println(rs.getInt("cantidad") + "\t" + rs.getString("codigo") + "\t" +
          rs.getString("descripcion") + "\t" + rs.getDouble("monto") + "\t" + rs.getDouble("subtotal"));
    }
  }

  public static void main(String[] args) {
    
    try {
      // conexión
      Class.forName("com.mysql.cj.jdbc.Driver");
      dbConnection = DriverManager.getConnection("jdbc:mysql://" + Constantes.getUrl() + "/" + 
          Constantes.getDBName(), Constantes.getUser(), Constantes.getPassword());
      
      // productos
      FacturaProducto fps[] = new FacturaProducto[3];
      fps[0] = new FacturaProducto(9, 1);
      fps[1] = new FacturaProducto(3, 1);
      fps[2] = new FacturaProducto(14, 1);
      
      // precio total
      double total = calcTotal(fps);
      System.out.println(total);
      
      if (total <= 100) {
        System.out.println("el total que se intenta facturar es " + total + ", menor que 100");
        return;
      }
      
      // factura
      Factura factura = new Factura(4, new Date(), 20);
      int idFactura = insertarFactura(factura);
      System.out.println("idFactura: " + idFactura);
      
      // productos
      insertarProductos(idFactura, fps);
      
      mostrarFactura(idFactura);
      
      dbConnection.close();
      
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    
    } catch (SQLException e) {
      e.printStackTrace();
    
    } catch (Exception e) {
      e.printStackTrace();
    }
    
  }

}
