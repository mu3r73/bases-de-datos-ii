package tp1_ej12_statement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import tp1_config.Constantes;
import tp1_model.Cliente;


public class Main {

  private static Connection dbConnection = null;

  private static void insertarCliente(Cliente cliente) throws SQLException {
    Statement statement = dbConnection.createStatement();
    String query = "insert into cliente (codigo, apellido, nombre) values ('" + cliente.getCodigo() +
        "', '" + cliente.getApellido() + "', '" + cliente.getNombre() + "')";
    System.out.println(query);
    statement.execute(query);
    statement.close();    
  }

  private static void mostrarClientes() throws SQLException {
    Statement statement = dbConnection.createStatement();
    String query = "select * from cliente";
    ResultSet rs = statement.executeQuery(query);
    System.out.println("id\tcódigo\tapellido\tnombre");
    while (rs.next()) {
      System.out.println(rs.getInt("id_cliente") + "\t" + rs.getString("codigo") + "\t" +
          rs.getString("apellido") + "\t" + rs.getString("nombre"));
    }
    statement.close();    
  }

  public static void main(String[] args) {
    
    try {
      // conexión
      Class.forName("com.mysql.cj.jdbc.Driver");
      dbConnection = DriverManager.getConnection("jdbc:mysql://" + Constantes.getUrl() + "/" + 
          Constantes.getDBName(), Constantes.getUser(), Constantes.getPassword());
      
      // insert
      Cliente cliente = new Cliente("C101", "Salgado", "Mariela");
      insertarCliente(cliente);
      
      // query
      mostrarClientes();
      
      dbConnection.close();

    } catch (SQLException e) {
      e.printStackTrace();
    
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    
  }

}
