package tp1_model;

public class Cliente {

  private Integer id_cliente;
  private String codigo;
  private String apellido;
  private String nombre;
  
  public Cliente(String codigo, String apellido, String nombre) {
    super();
    this.codigo = codigo;
    this.apellido = apellido;
    this.nombre = nombre;
  }

  public Integer getId_cliente() {
    return id_cliente;
  }

  public void setId_cliente(Integer id_cliente) {
    this.id_cliente = id_cliente;
  }

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getApellido() {
    return apellido;
  }

  public void setApellido(String apellido) {
    this.apellido = apellido;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  
}
