package tp1_model;

import java.util.Date;

public class Factura {
  
  private Integer idFactura;
  private Integer idCliente;
  private Date fecha;
  private Integer numero;
  
  public Factura(Integer idCliente, Date fecha, Integer numero) {
    super();
    this.idCliente = idCliente;
    this.fecha = fecha;
    this.numero = numero;
  }

  public Integer getIdFactura() {
    return idFactura;
  }

  public void setIdFactura(Integer idFactura) {
    this.idFactura = idFactura;
  }

  public Integer getIdCliente() {
    return idCliente;
  }

  public void setIdCliente(Integer idCliente) {
    this.idCliente = idCliente;
  }

  public Date getFecha() {
    return fecha;
  }

  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  public Integer getNumero() {
    return numero;
  }

  public void setNumero(Integer numero) {
    this.numero = numero;
  }
  
}
