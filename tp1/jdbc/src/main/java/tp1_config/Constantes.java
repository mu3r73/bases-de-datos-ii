package tp1_config;


public class Constantes {
  
  static final boolean PRODUCTION = true;
  
  static final String DB_NAME = "tpventas";
  
  static final String DEV_URL = "192.168.56.103:3306";
  static final String DEV_USER = "muerte";
  static final String DEV_PASSWORD = "gojira";

  static final String PROD_URL = "localhost:3306";
  static final String PROD_USER = "admin";
  static final String PROD_PASSWORD = "lamadrid";

  public static String getUrl() {
    return Constantes.PRODUCTION ? Constantes.PROD_URL : Constantes.DEV_URL;
  }
  
  public static String getUser() {
    return Constantes.PRODUCTION ? Constantes.PROD_USER : Constantes.DEV_USER;
  }
  
  public static String getPassword() {
    return Constantes.PRODUCTION ? Constantes.PROD_PASSWORD : Constantes.DEV_PASSWORD;
  }
  
  public static String getDBName() {
    return Constantes.DB_NAME;
  }
  
}
