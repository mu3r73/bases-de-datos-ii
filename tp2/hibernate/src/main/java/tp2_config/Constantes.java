package tp2_config;


public class Constantes {
  
  static final boolean PRODUCTION = true;
  
  static final String DB_NAME = "tp2";
  
  static final String DEV_BASE_URL = "192.168.56.103:3306";
  static final String DEV_USER = "muerte";
  static final String DEV_PASSWORD = "gojira";

  static final String PROD_BASE_URL = "localhost:3306";
  static final String PROD_USER = "admin";
  static final String PROD_PASSWORD = "lamadrid";

  
  public static String getUrl() {
    return "jdbc:mysql://" + Constantes.getBaseUrl() + "/" + Constantes.getDBName();
  }
  
  private static String getBaseUrl() {
    return Constantes.PRODUCTION ? Constantes.PROD_BASE_URL : Constantes.DEV_BASE_URL;
  }
  
  private static String getDBName() {
    return Constantes.DB_NAME;
  }

  public static String getUser() {
    return Constantes.PRODUCTION ? Constantes.PROD_USER : Constantes.DEV_USER;
  }
  
  public static String getPassword() {
    return Constantes.PRODUCTION ? Constantes.PROD_PASSWORD : Constantes.DEV_PASSWORD;
  }
    
}
