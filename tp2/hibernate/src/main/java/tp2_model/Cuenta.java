package tp2_model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "cuenta")
public class Cuenta {
  
  @Id
  @GeneratedValue
  private long id;  

  @Column(nullable = false, unique = true, updatable = false)
  private String numero;  

  @OneToOne
  @JoinColumn(name = "fk_id_cliente", foreignKey = @ForeignKey(name = "fk_id_cuenta"))
  private Cliente cliente;
  
  // constructores
  public Cuenta() {
    super();
  }

  public Cuenta(String numero, Cliente cliente) {
    super();
    this.numero = numero;
    this.cliente = cliente;
  }

  // overrides
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (!(obj instanceof Cuenta))
      return false;
    Cuenta other = (Cuenta) obj;
    return Objects.equals(id, other.id);
  }
  
  @Override
  public String toString() {
    return this.numero + " - cliente: " + this.cliente.getCodigo();
  }
  
  // getters y setters
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  public Cliente getCliente() {
    return cliente;
  }

  public void setCliente(Cliente cliente) {
    this.cliente = cliente;
  }
  
}
