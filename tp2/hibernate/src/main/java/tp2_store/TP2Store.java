package tp2_store;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import tp2_config.HibernateUtils;
import tp2_model.Cliente;
import tp2_model.Cuenta;
import tp2_model.Detalle;
import tp2_model.Factura;
import tp2_model.Precio;
import tp2_model.Producto;
import tp2_model.Proveedor;


public class TP2Store {
  
  private Session session;

  /**
   * sesión
   */
  
  public void startSession(String hbm2ddl) {
    HibernateUtils.buildSessionFactory(hbm2ddl);
    this.session = HibernateUtils.getSessionFactory().openSession();
  }
  
  public void closeSession() {
    this.session.close();
    HibernateUtils.closeSessionFactory();
  }
  
  public boolean isSessionOpen() {
    return this.session.isOpen();
  }

  /**
   *  altas
   */
  
  public void addCliente(Cliente c) {
    Transaction t = null;

    try {
      System.out.println(">>> store: agregando cliente: " + c);
      t = this.session.beginTransaction();
      
      this.session.persist(c);
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace();
    }
  }
  
  public void addClientes(List<Cliente> cs) {
    Transaction t = null;

    try {
      System.out.println(">>> store: alta de clientes");
      t = this.session.beginTransaction();
      
      for (Cliente c : cs) {
        System.out.println(">>> store: agregando cliente: " + c);
        this.session.persist(c);
      }
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace();
    }
  }
  
  public void addProveedor(Proveedor p) {
    Transaction t = null;

    try {
      System.out.println(">>> store: agregando proveedor: " + p);
      t = this.session.beginTransaction();

      this.session.persist(p);
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace(); 
    }
  }
  
  public void addProveedores(List<Proveedor> ps) {
    Transaction t = null;

    try {
      System.out.println(">>> store: alta de proveedores");
      t = this.session.beginTransaction();

      for (Proveedor p : ps) {
        System.out.println(">>> store: agregando proveedor: " + p);
        this.session.persist(p);
      }
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace(); 
    }
  }
  
  public void addProducto(Producto p) {
    Transaction t = null;

    try {
      System.out.println(">>> store: agregando producto: " + p);
      t = this.session.beginTransaction();
      
      this.session.persist(p);
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace();
    }
  }
  
  public void addProductos(List<Producto> ps) {
    Transaction t = null;

    try {
      System.out.println(">>> store: alta de productos");
      t = this.session.beginTransaction();
      
      for (Producto p : ps) {
        System.out.println(">>> store: agregando producto: " + p);
        this.session.persist(p);
      }
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace();
    }
  }
  
  public void addFactura(Factura f) {
    Transaction t = null;

    try {
      System.out.println(">>> store: agregando factura: " + f);
      t = this.session.beginTransaction();
      
      this.session.persist(f);
      
      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace();
    }
  }
  
  /**
   * modificaciones
   */
  
  public void updatePrecio(String codProducto, BigDecimal monto) {
    Transaction t = null;

    try {
      t = this.session.beginTransaction();

      System.out.println(">>> store: actualización de precio");

      Producto p = this.fetchProducto(codProducto);
      p.setPrecio(monto);
      System.out.println(">>> store: actualizando producto: " + p);
      this.session.merge(p);

      this.session.flush();
      t.commit();

    } catch (Exception e) {
      t.rollback();
      e.printStackTrace();
    }
  }
  
  /**
   *  consultas
   */
  
  @SuppressWarnings("unchecked")
  public List<Cliente> fetchClientes() {
    Query q = this.session.createQuery("from Cliente");
    return q.getResultList();
  }
  
  @SuppressWarnings("unchecked")
  public List<Cuenta> fetchCuentas() {
    Query q = this.session.createQuery("from Cuenta");
    return q.getResultList();
  }
  
  @SuppressWarnings("unchecked")
  public List<Proveedor> fetchProveedores() {
    Query q = this.session.createQuery("from Proveedor");
    return q.getResultList();
  }
  
  @SuppressWarnings("unchecked")
  public List<Producto> fetchProductos() {
    Query q = this.session.createQuery("from Producto");
    return q.getResultList();
  }
  
  @SuppressWarnings("unchecked")
  public List<Precio> fetchPrecios() {
    Query q = this.session.createQuery("from Precio");
    return q.getResultList();
  }
  
  public Proveedor fetchProveedor(String desc) {
    Query q = this.session.createQuery("from Proveedor where descripcion = :desc");
    q.setParameter("desc", desc);
    return (Proveedor) q.getResultList().get(0);
  }
  
  @SuppressWarnings("unchecked")
  public Collection<Precio> fetchPrecios(String codProducto) {
    Query q = this.session.createQuery("from Precio p join fetch p.producto prod "
        + "where prod.codigo=:codProducto");
    q.setParameter("codProducto", codProducto);
    return q.getResultList();
  }
  
  public Producto fetchProducto(String cod) {
    Query q = this.session.createQuery("from Producto where codigo = :cod");
    q.setParameter("cod", cod);
    return (Producto) q.getResultList().get(0);
  }

  public Cliente fetchCliente(String cod) {
    Query q = this.session.createQuery("from Cliente where codigo = :cod");
    q.setParameter("cod", cod);
    return (Cliente) q.getResultList().get(0);
  }

  public Factura fetchFactura(int num) {
    Query q = this.session.createQuery("from Factura where numero = :num");
    q.setParameter("num", num);
    return (Factura) q.getResultList().get(0);
  }

  @SuppressWarnings("unchecked")
  public List<Detalle> fetchDetalles(Integer numFactura) {
    Query q = this.session.createQuery("from Detalle d join fetch d.factura fac "
        + "where fac.numero = :numFactura");
    q.setParameter("numFactura", numFactura);
    return q.getResultList();
  }
  
}
