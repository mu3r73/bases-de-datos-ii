package tp2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import tp2_model.Cliente;
import tp2_model.Factura;
import tp2_model.Producto;
import tp2_model.Proveedor;
import tp2_store.TP2Store;

public class TP2TestData {
  
  private static TP2Store s = new TP2Store();
  
  /**
   * datos de prueba
   */
  
  public static List<Cliente> getClientes() {
    List<Cliente> cs = new ArrayList<>();
    cs.add(new Cliente("C001", "Giménez", "Verna Lucrecia", "7712"));
    cs.add(new Cliente("C002", "Aráoz", "Héctor", "3561"));
    cs.add(new Cliente("C003", "Chávez", "Alicia", "9906"));
    cs.add(new Cliente("C004", "Velazco", "Evaristo Gregorio", "5829"));
    return cs;
  }
  
  public static List<Proveedor> getProveedores() {
    List<Proveedor> ps = new ArrayList<>();
    ps.add(new Proveedor("Wingear S.A."));
    ps.add(new Proveedor("Dencor S.R.L."));
    ps.add(new Proveedor("Gaures Hermanos"));
    ps.add(new Proveedor("Ainmata Mayorista"));
    return ps;
  }
  
  public static List<Producto> getProductos() {
    s.startSession("none");
    
    List<Producto> prods = new ArrayList<>();
    
    // 1 proveedor, tiene precio
    prods.add(new Producto("P001", "Conector de cadena KMC Missing Link",
                          s.fetchProveedor("Wingear S.A."),
                          Optional.of(BigDecimal.valueOf(180))));
    // 1 proveedor, sin precio
    prods.add(new Producto("P002", "Pata SRAM GX Eagle",
                          s.fetchProveedor("Dencor S.R.L."),
                          Optional.empty()));
    // 3 proveedores, con precio
    Producto p3 = new Producto("P003", "Horquilla Suspensión Rock Shox Tora Rod.26",
        s.fetchProveedor("Gaures Hermanos"),
        Optional.of(BigDecimal.valueOf(4790)));
    Collection<Proveedor> ps3 = p3.getProveedores();
    ps3.add(s.fetchProveedor("Wingear S.A."));
    ps3.add(s.fetchProveedor("Ainmata Mayorista"));
    p3.setProveedores(ps3);
    prods.add(p3);
    
    // 2 proveedores, sin precio
    Producto p4 = new Producto("P004", "Piñón Shimano Deore Cassette M6000",
        s.fetchProveedor("Ainmata Mayorista"),
        Optional.empty());
    Collection<Proveedor> ps4 = p4.getProveedores();
    ps4.add(s.fetchProveedor("Dencor S.R.L."));
    p4.setProveedores(ps4);
    prods.add(p4);
    
    s.closeSession();
    
    return prods;
  }

  public static Factura getFactura() {
    s.startSession("none");
    
    Cliente c = s.fetchCliente("C003");
    
    Factura f = new Factura(c, 1);
    
    Producto p1 = s.fetchProducto("P001");
    f.addDetalle(p1, 2);
    
    Producto p3 = s.fetchProducto("P003");
    f.addDetalle(p3, 1);
    
    s.closeSession();
    
    return f;
  }

}
