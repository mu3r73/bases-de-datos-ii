package tp2;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tp2_model.Precio;
import tp2_model.Producto;
import tp2_model.Proveedor;
import tp2_store.TP2Store;

public class TP2Ej8TestActualizPrecio {
  
  private static Logger l;
  private static TP2Store s;

  @BeforeClass
  public static void config() {
    l = LogManager.getLogger(TP2Ej8TestActualizPrecio.class);
    s = new TP2Store();
  }

  @Test
  public void altaProductos() {
    l.info(">>> tp2ej8: inicialización");
    s.startSession("create");

    List<Proveedor> provs = TP2TestData.getProveedores();
    s.addProveedores(provs);
    
    List<Producto> prods = TP2TestData.getProductos();
    s.addProductos(prods);
    
    s.updatePrecio("P001", BigDecimal.valueOf(205));
    
    validarCarga();

    s.closeSession();
    Assert.assertFalse(s.isSessionOpen());    
  }

  private static void validarCarga() {
    l.info(">>> tp2ej8: consulta de producto");
    Producto p = s.fetchProducto("P001");
    Assert.assertTrue(p.getPrecio().getMonto().toString().equals("205"));

    l.info(">>> tp2ej8: consulta de precios");
    Collection<Precio> ps = s.fetchPrecios("P001");
    Assert.assertEquals(2, ps.size());
  }
  
  @AfterClass
  public static void close() {
    l.info(">>> tp2ej8: terminado");
  }

}
