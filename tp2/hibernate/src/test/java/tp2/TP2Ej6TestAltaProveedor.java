package tp2;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tp2_model.Proveedor;
import tp2_store.TP2Store;

public class TP2Ej6TestAltaProveedor {

  private static Logger l;
  private static TP2Store s;

  @BeforeClass
  public static void config() {
    l = LogManager.getLogger(TP2Ej6TestAltaProveedor.class);
    s = new TP2Store();
  }

  @Test
  public void altaProveedores() {
    l.info(">>> tp2ej6: inicialización");
    s.startSession("create");
    
    List<Proveedor> ps = TP2TestData.getProveedores();
    s.addProveedores(ps);
    
    validarCarga(ps);

    s.closeSession();
    Assert.assertFalse(s.isSessionOpen());
  }

  private static void validarCarga(List<Proveedor> ps) {
    l.info(">>> tp2ej6: consulta de proveedores");
    
    // todos los proveedores en ps están cargados
    List<Proveedor> provRes = s.fetchProveedores();
    for (Proveedor p : ps) {
      Assert.assertTrue(provRes.contains(p));
    }
    // hay un total de 4 proveedores
    Assert.assertEquals(ps.size(), provRes.size());
  }

  @AfterClass
  public static void close() {
    l.info(">>> tp2ej6: terminado");
  }

}
