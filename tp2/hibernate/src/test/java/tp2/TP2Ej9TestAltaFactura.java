package tp2;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tp2_model.Cliente;
import tp2_model.Detalle;
import tp2_model.Factura;
import tp2_model.Producto;
import tp2_model.Proveedor;
import tp2_store.TP2Store;

public class TP2Ej9TestAltaFactura {
  
  private static Logger l;
  private static TP2Store s;

  @BeforeClass
  public static void config() {
    l = LogManager.getLogger(TP2Ej9TestAltaFactura.class);
    s = new TP2Store();
  }
  
  @Test
  public void altaFactura() {
    l.info(">>> tp2ej9: inicialización");
    s.startSession("create");

    List<Cliente> cs = TP2TestData.getClientes();
    s.addClientes(cs);
    
    List<Proveedor> provs = TP2TestData.getProveedores();
    s.addProveedores(provs);
    
    List<Producto> prods = TP2TestData.getProductos();
    s.addProductos(prods);
    
    Factura f = TP2TestData.getFactura();
    s.addFactura(f);
    
    validarCarga(f);

    s.closeSession();
    Assert.assertFalse(s.isSessionOpen());
  }
  
  private static void validarCarga(Factura f) {
    l.info(">>> tp2ej9: consulta de factura");
    Factura fRes = s.fetchFactura(1);
    
    // el cliente de fRes es el mismo que se cargó en f
    Assert.assertEquals(fRes.getCliente(), f.getCliente());
    // la fecha de fRes es la misma que se cargó en f
    Assert.assertEquals(fRes.getFecha(), f.getFecha());
    // los detalles de fRes son los mismos que se cargaron en f
    for (Detalle d : f.getDetalles()) {
      Assert.assertTrue(fRes.getDetalles().contains(d));
    }
    
    l.info(">>> tp2ej9: consulta de detalles");
    List<Detalle> ds = s.fetchDetalles(f.getNumero());
    // hay un detalle para cada detalle en f
    for (Detalle d : f.getDetalles()) {
      Assert.assertTrue(ds.contains(d));
    }
    // los precios son correctos
    for (Detalle d : ds) {
      Assert.assertTrue(d.getPrecio().equals(d.getProducto().getPrecio()));
    }
  }

  @AfterClass
  public static void close() {
    l.info(">>> tp2ej9: terminado");
  }
  
}
