package tp2;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tp2_model.Cliente;
import tp2_model.Cuenta;
import tp2_store.TP2Store;

public class TP2Ej5TestAltaCliente {

  private static Logger l;
  private static TP2Store s;
  
  @BeforeClass
  public static void config() {
    l = LogManager.getLogger(TP2Ej5TestAltaCliente.class);
    s = new TP2Store();
  }

  @Test
  public void altaClientes() {
    l.info(">>> tp2ej5: inicialización");
    s.startSession("create");
    
    List<Cliente> cs = TP2TestData.getClientes();
    s.addClientes(cs);
    
    validarCarga(cs);
    
    s.closeSession();
    Assert.assertFalse(s.isSessionOpen());
  }
  
  private static void validarCarga(List<Cliente> cs) {
    l.info(">>> tp2ej5: consulta de clientes");
    
    // todos los clientes en cs están cargados
    List<Cliente> cliRes = s.fetchClientes();
    for (Cliente c : cs) {
      Assert.assertTrue(cliRes.contains(c));
    }
    // hay un total de 4 clientes
    Assert.assertEquals(cs.size(), cliRes.size());

    l.info(">>> tp2ej5: consulta de cuentas");
    // hay una cuenta para cada cliente en cs
    List<Cuenta> cuRes = s.fetchCuentas();
    for (Cliente c : cs) {
      Assert.assertTrue(cuRes.stream().anyMatch(cue -> cue.getCliente().equals(c)));
    }
    // hay un total de 4 cuentas
    Assert.assertEquals(cs.size(), cuRes.size());
  }

  @AfterClass
  public static void close() {
    l.info(">>> tp2ej5: terminado");
  }

}
