package tp2;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tp2_model.Precio;
import tp2_model.Producto;
import tp2_model.Proveedor;
import tp2_store.TP2Store;

public class TP2Ej7TestAltaProducto {

  private static Logger l;
  private static TP2Store s;

  @BeforeClass
  public static void config() {
    l = LogManager.getLogger(TP2Ej7TestAltaProducto.class);
    s = new TP2Store();
  }

  @Test
  public void altaProductos() {
    l.info(">>> tp2ej7: inicialización");
    s.startSession("create");
    
    List<Proveedor> provs = TP2TestData.getProveedores();
    s.addProveedores(provs);
    
    List<Producto> prods = TP2TestData.getProductos();
    s.addProductos(prods);
    
    validarCarga(prods);

    s.closeSession();
    Assert.assertFalse(s.isSessionOpen());    
  }

  private static void validarCarga(List<Producto> prods) {
    l.info(">>> tp2ej7: consulta de productos");
    
    // todos los productos en prods están cargados
    List<Producto> prodsRes = s.fetchProductos();
    for (Producto p : prods) {
      Assert.assertTrue(prodsRes.contains(p));
    }
    // hay un total de 4 productos
    Assert.assertEquals(prods.size(), prodsRes.size());

    l.info(">>> tp2ej7: consulta de precios");
    // hay un precio para cada producto en prods que debiera tener precio
    List<Precio> presRes = s.fetchPrecios();
    for (Producto p : prods) {
      if (p.getPrecio() != null) {
        Assert.assertTrue(presRes.stream().anyMatch(pre -> pre.getProducto().equals(p)));
      }
    }
    // hay un total de 2 precios
    Assert.assertEquals(2, presRes.size());
  }
  
  @AfterClass
  public static void close() {
    l.info(">>> tp2ej7: terminado");
  }
  
}
